# StarterKit

Python 3.8, Django 3.0.x

# How to start

- clone project 
- run `pipenv install --dev`
- run `pre-commit install`

# Install
For the next steps of service installation, you will need to setup Ubuntu OS.



## Manual installation for local development

* Clone this repository to your local machine
```commandline
    git clone https://gitlab.com/starnavi-team/startkit/server-django.git
    cd dashboard_project/
```

* Add directory to PYTHONPATH
```commandline
    PYTHONPATH=$PYTHONPATH:`pwd`
    export PYTHONPATH
```

* Activate virtual environment
```commandline
    pipenv shell
```

* Install packages for local development
```commandline
    pipenv install --dev
```
####DB
* if you want to use sqlite you have to create sqlite_local folder in the root folder of the project or you 
must define your own path or even different DB type with DATABASE_URL environment variable.
Example: 
```commandline
    postgres://USER:PASSWORD@HOST:PORT/NAME
    postgis://USER:PASSWORD@HOST:PORT/NAME
    mysql://USER:PASSWORD@HOST:PORT/NAME
*** sqlite:////PATH 

```
*** (four slashes) for sqlite full path will look like this: sqlite:////full/path/to/your/database/file.sqlite

details here:Link [https://github.com/jacobian/dj-database-url/](https://github.com/jacobian/dj-database-url) 

* Run migrate files
```
    python manage.py migrate
```

* Run Django development server
( By default, the server runs on port 8000 on the IP address 127.0.0.1.)

```
python manage.py runserver
```
Link: [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


#### **Clear database**
```commandline
python manage.py flush
```

#### **For tests launch**
```
python manage.py test
```
### Deploy into local docker environment
- clone project 
    
```commandline
    git clone https://gitlab.com/starnavi-team/startkit/server-django.git 
    cd dashboard_project/
```
### Edit local .env file or use environment variables
```commandline
    
    EMAIL_HOST=smtp.host.com
    EMAIL_PORT=587
    EMAIL_HOST_USER=user@host.com
    EMAIL_HOST_PASSWORD=PassWD!
    EMAIL_USE_TLS=True
    EMAIL_USE_SSL=False
    EMAIL_TIMEOUT=300
    DATABASE_URL=sqlite:////opt/app/sqlite_local/db.sqlite

    #STRIPE_SECRET_KEY= .....
    #STRIPE_WEBHOOK_KEY= .....
```

### Run container 
* run and django-server image
```commandline
    docker-compose --project-directory . -f ./local_docker/docker-compose.yml up -d  
```
* stop db and django-server images
```commandline
    docker-compose --project-directory . -f ./local_docker/docker-compose.yml down  
```
* Docker desktop for windows can't apply right permission on the db folder. To avoid this problem we have to use docker volumes.
So to fully remove containers data if you do not need them anymore run:
```commandline
    docker system prune -f -a --volumes
```

### to test deploy result 

Link: [http://localhost:8000/api/v1/documentation/](http://localhost:8000/api/v1/documentation/)
