import re
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.views import serve
from django.urls import include, re_path
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from django.conf.urls.static import static
from dj_rest_auth.registration.views import SocialAccountListView
from django.views.generic import TemplateView

urlpatterns = [

    path('api/v1/', include('accounts.urls')),
    path('api/v1/authentication/', include('accounts.authentication_urls')),
    path('api/v1/social_authentication/social_accounts/', SocialAccountListView.as_view(), name='socialaccount_signup'),
    path('api/v1/social_authentication/', include('accounts.social_accounts_urls')),
    path('api/v1/registration/', include('accounts.registration_urls')),
    path('api/v1/billing/', include('billing.urls')),
    path('site_administrator_panel/', admin.site.urls),
    re_path(r"^user-activation/(?P<key>[-:\w]+)/$", TemplateView.as_view(), name="account_confirm_email"),
    re_path(r'^%s(?P<path>.*)$' % re.escape(settings.STATIC_URL.lstrip('/')),
            lambda request, path, insecure=True, **kwargs: serve(request, path, insecure, **kwargs)
            ),
]

# API documentation
schema_view = get_schema_view(
    openapi.Info(
        title='Dashboard API',
        default_version='v1',
        description='Dashboard API',
        terms_of_service='https://www.google.com/policies/terms/',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


def trigger_error(request):
    division_by_zero = 1 / 0
    assert division_by_zero


urlpatterns += [
                   path('sentry-debug/', trigger_error),
                   path(
                       'api/v1/documentation/',
                       schema_view.with_ui('redoc', cache_timeout=0),
                       name='schema-redoc',
                   ),
               ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
               static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
