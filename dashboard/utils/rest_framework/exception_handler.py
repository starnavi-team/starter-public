from rest_framework.views import exception_handler
from rest_framework.exceptions import ValidationError


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    response_data = getattr(response, 'data', None)
    if all([response_data, isinstance(exc, ValidationError), isinstance(response_data, list)]):
        if len(response_data) == 1:
            response.data = {'detail': response_data[0]}
    return response
