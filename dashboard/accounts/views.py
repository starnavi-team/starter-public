from accounts import serializers
from accounts.helpers import send_invitation
from billing import models as billing_models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import logout as django_logout
from django.contrib.auth import get_user_model
from dj_rest_auth.registration.views import SocialLoginView, VerifyEmailView as DjRestAuthVerifyEmailView
from rest_framework import mixins
from rest_framework import response
from rest_framework import viewsets
from rest_framework import views
from rest_framework import exceptions
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

User = get_user_model()


class UserViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

    def get_object(self):
        return self.request.user

    def list(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_summary='Remove users account ',
        operation_description='User removes his account',
        request_body=serializers.UserDeleteConfirmationSerializer,
        responses={status.HTTP_204_NO_CONTENT: ''}
    )
    @action(methods=['post'], detail=False, url_path='delete-my-account', url_name='delete_my_account')
    def delete_my_account(self, request, *args, **kwargs):
        context = self.get_serializer_context()
        serializer = serializers.UserDeleteConfirmationSerializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)

        user = request.user
        active_subscriptions = billing_models.Subscription.objects.filter(
            user=user,
            status__in=billing_models.SUBSCRIPTION_NEED_CANCELLATION_STATUSES
        )
        if active_subscriptions:
            raise exceptions.ValidationError(
                {
                    'subscriptions': 'You have active subscriptions'
                }
            )

        try:
            request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        if getattr(settings, 'REST_SESSION_LOGIN', True):
            django_logout(request)

        final_response = response.Response(status=status.HTTP_204_NO_CONTENT)
        if getattr(settings, 'REST_USE_JWT', False):

            cookie_name = getattr(settings, 'JWT_AUTH_COOKIE', None)
            if cookie_name:
                final_response.delete_cookie(cookie_name)

        self.perform_user_delete(user)

        return final_response

    def perform_user_delete(self, user):
        billing_models.Subscription.objects.filter(user=user).delete()
        user.delete()


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class ConfirmEmailView(DjRestAuthVerifyEmailView):
    def process_confirmation(self, request) -> User:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.kwargs['key'] = serializer.validated_data['key']
        confirmation = self.get_object()
        confirmation.confirm(self.request)
        return confirmation.email_address.user

    def post(self, request, *args, **kwargs):
        user = self.process_confirmation(request)
        # callback-like call to move the business logic inside the User class
        user.on_registration_complete()
        return response.Response({'detail': 'ok'}, status=status.HTTP_200_OK)


class SendInvitationView(views.APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Send invitation',
        operation_description='User can send invitation to the other user by email',
        request_body=serializers.SendInvitationSerializer(),
        responses={
            status.HTTP_200_OK:
                openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'detail': openapi.Schema('The invitation has sent', type=openapi.TYPE_STRING)
                    }
                )
        }
    )
    def post(self, request):
        serializer = serializers.SendInvitationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_invitation(serializer.validated_data['invited_email'], request)
        return response.Response({'detail': 'The invitation has sent'}, status=status.HTTP_200_OK)
