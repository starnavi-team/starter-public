from django.urls import path
from dj_rest_auth import views as dj_rest_auth_views
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

urlpatterns = [

    path(
        'login/',
        dj_rest_auth_views.LoginView.as_view(),
        name='rest_login',
    ),

    path(
        'token/refresh/',
        TokenRefreshView.as_view(),
        name='token_refresh',
    ),
    path(
        'token/verify/',
        TokenVerifyView.as_view(),
        name='token_verify'
    ),

    path(
        'logout/',
        dj_rest_auth_views.LogoutView.as_view(),
        name='rest_logout',
    ),
    path(
        'password/change/',
        dj_rest_auth_views.PasswordChangeView.as_view(),
        name='rest_password_change',
    ),
    path(
        'password/reset/',
        dj_rest_auth_views.PasswordResetView.as_view(),
        name='rest_password_reset',
    ),
    path(
        'password/reset/confirm/',
        dj_rest_auth_views.PasswordResetConfirmView.as_view(),
        name='rest_password_reset_confirm',
    ),
]
