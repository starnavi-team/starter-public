from django.urls import path

from accounts import views


urlpatterns = [
    path('google/', views.GoogleLogin.as_view(), name='google_login'),
    path('facebook/', views.FacebookLogin.as_view(), name='fb_login'),
]
