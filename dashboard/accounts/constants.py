ENGLISH = 'english'
RUSSIAN = 'russian'

LANGUAGES = (
    (ENGLISH, 'English'),
    (RUSSIAN, 'Russian'),
)
