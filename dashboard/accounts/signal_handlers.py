from django.db.models.signals import pre_delete
from django.dispatch import receiver
from accounts import models


@receiver(pre_delete, sender=models.User)
def clean_invited_by(sender, instance: models.User, using, **kwargs):
    users = [user for user in models.User.objects.filter(invited_by=instance)]
    if users:
        for user in users:
            user.invited_by = None

        models.User.objects.bulk_update(users, ['invited_by'], batch_size=1000)
