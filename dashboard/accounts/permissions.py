"""Custom permissions for accounts."""
from rest_framework import permissions


class IsOwnerPermission(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit and see it.
    """

    def has_object_permission(self, request, view, instance):
        return instance.user == request.user
