from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.models import EmailAddress
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError


class UserSocialAccountAdapter(DefaultSocialAccountAdapter):
    def save_user(self, request, sociallogin, form=None):
        # we should remember state here, because after save_user there is always saved user instance
        is_signup = sociallogin.user.pk is None

        ret = super().save_user(request, sociallogin, form)

        is_verified = EmailAddress.objects.filter(user=sociallogin.user, verified=True, primary=True).exists()

        if is_signup and is_verified:
            sociallogin.user.on_registration_complete(True)

        return ret

    def pre_social_login(self, request, sociallogin):

        if settings.SOCIALACCOUNT_EMAIL_REQUIRED:
            mail = getattr(sociallogin.user, 'email', None)
            if not mail:
                raise ValidationError(_('E-mail is required. Please add an e-mail to your social account'))


class UserAccountAdapter(DefaultAccountAdapter):

    def login(self, request, user):

        if settings.SOCIALACCOUNT_EMAIL_VERIFICATION == 'mandatory':
            email_address = user.emailaddress_set.get(email=user.email, primary=True)
            if not email_address.verified:
                raise ValidationError(_('E-mail is not verified.'))

        super(UserAccountAdapter, self).login(request, user)
