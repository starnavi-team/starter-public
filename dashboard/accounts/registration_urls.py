from django.urls import path
from dj_rest_auth.registration.views import RegisterView, VerifyEmailView

from .views import ConfirmEmailView

urlpatterns = [
    path('', RegisterView.as_view(), name='rest_register'),
    path('verify-email/', ConfirmEmailView.as_view(), name='rest_verify_email'),
    path('account_confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
]
