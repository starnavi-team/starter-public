import secrets
import base64


def generate_referral_code(email=None):
    random_part = secrets.token_urlsafe(12)
    email_part = base64.urlsafe_b64encode(email.encode('ascii')).rstrip(b'=').decode('ascii')[:12]
    return random_part + email_part
