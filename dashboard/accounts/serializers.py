from django.contrib.auth import get_user_model
from dj_rest_auth.registration.serializers import RegisterSerializer
from dj_rest_auth.serializers import LoginSerializer
from dj_rest_auth.serializers import UserDetailsSerializer
from dj_rest_auth.serializers import PasswordResetSerializer
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from accounts import forms
from allauth.account.models import EmailAddress

UserModel = get_user_model()


class UserSerializer(UserDetailsSerializer):
    """Serializer for User."""

    class Meta:
        model = UserModel
        fields = (
            'id', 'email', 'first_name', 'last_name', 'avatar', 'created_at', 'language', 'own_referral')

        read_only_fields = ('email', 'own_referral')


class UserDeleteConfirmationSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=128)

    def validate_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            err_msg = _("Your password was entered incorrectly. Please enter it again.")
            raise serializers.ValidationError(err_msg)
        return value


class CustomUserRegisterSerializer(RegisterSerializer):
    username = None
    password1 = serializers.CharField(
        write_only=True, label='Enter your password',
        style={'input_type': 'password'},
    )
    password2 = serializers.CharField(
        write_only=True, label='Confirm password',
        style={'input_type': 'password'},
    )
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)

    invited_with = serializers.CharField(required=False)

    def get_cleaned_data(self):
        return {
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
        }

    def custom_signup(self, request, user):
        referral_code = self.validated_data.get('invited_with', '')
        if referral_code:
            if inviter := UserModel.objects.filter(own_referral=referral_code).first():
                user.invited_by = inviter
                user.save()


class CustomUserLoginSerializer(LoginSerializer):
    username = None


class CustomPasswordResetSerializer(PasswordResetSerializer):
    password_reset_form_class = forms.CustomResetPasswordForm


class SendInvitationSerializer(serializers.Serializer):
    invited_email = serializers.EmailField(required=True)

    def validate(self, attrs):
        email = attrs.get('invited_email')
        if EmailAddress.objects.filter(email=email).exists():
            raise serializers.ValidationError({'invited_email': 'User with this email is already our customer'})
        return attrs
