from django.core.mail import EmailMultiAlternatives
from rest_framework import exceptions
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.template import loader
import base64


def _generate_message_body(template_name: str, data: dict):
    return loader.render_to_string(template_name, context=data)


def send_email(message_subject, message_text, message_html, email_to) -> None:
    email_message = EmailMultiAlternatives(message_subject, message_text, settings.DEFAULT_FROM_EMAIL, [email_to])
    email_message.attach_alternative(message_html, "text/html")

    try:
        email_message.send()
    except Exception:
        raise exceptions.ValidationError('The mail was not sent.')


def send_invitation(invited_email=None, request=None):
    current_site = get_current_site(request)
    protocol = 'https' if request.is_secure() else 'http'
    url_safe_email = base64.urlsafe_b64encode(invited_email.encode('ascii')).rstrip(b'=').decode('ascii')
    context = {
        'invitation_url': f'{protocol}://{current_site}/invitation/{request.user.own_referral}:{url_safe_email}/',
        'friend_name': request.user.get_invitation_name(),
        'current_site': current_site
    }
    _message_subject = _generate_message_body('invitation/txt/invitation_email_subject.txt', context)
    _message_text = _generate_message_body('invitation/txt/invitation_email.txt', context)
    _message_html = _generate_message_body('invitation/html/invitation_email.html', context)
    send_email(_message_subject, _message_text, _message_html, invited_email)
