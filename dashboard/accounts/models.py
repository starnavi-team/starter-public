import hashlib
import os
import stripe
from accounts import constants
from accounts import services
from typing import Union
from allauth.account.models import EmailAddress
from stripe.error import StripeError
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager
from django.db import models
from common.storage import OverwriteStorage


def _generate_avatar_filename(instance, filename):
    # generates unique constant filename for user
    ext = filename.split('.')[-1]
    new_filename = f'{hashlib.md5((str(instance.id)).encode()).hexdigest()}.{ext}'
    return os.path.join('avatars', new_filename)


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    first_name = models.CharField(
        max_length=30,
    )
    last_name = models.CharField(
        max_length=150,
    )
    email = models.EmailField(
        unique=True, max_length=254, db_index=True,
    )
    avatar = models.ImageField(null=True, default=None, storage=OverwriteStorage(), upload_to=_generate_avatar_filename)

    created_at = models.DateTimeField(auto_now_add=True)

    language = models.CharField(
        choices=constants.LANGUAGES,
        default=constants.ENGLISH,
        max_length=30,
    )

    stripe_id = models.CharField(max_length=32, blank=True, default='', help_text='Stripe Customer ID')
    own_referral = models.CharField(max_length=32, unique=True, help_text='User`s own referral code')
    invited_by = models.ForeignKey("self", blank=True, null=True, on_delete=models.DO_NOTHING)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        db_table = 'users'
        ordering = ['-id']

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.own_referral = services.generate_referral_code(self.email)
        super().save(*args, **kwargs)

    def on_registration_complete(self, commit: bool = True) -> None:
        # ignore exception in order to not to fail the registration-completing process
        try:
            self.sync_stripe_customer(commit)
        except StripeError:
            pass

    # Billing methods bellow

    def sync_stripe_customer(self, commit: bool = True) -> stripe.Customer:
        Subscription = self._meta.fields_map['subscriptions'].related_model

        # check if user has Stripe customer
        if self.stripe_id:
            customer = stripe.Customer.retrieve(self.stripe_id)
            # also sync subscriptions
            Subscription.sync_user_subscription(self)
            return customer

        # check if there is a Customer in Stripe with email equal to the user's one
        customers_with_email = stripe.Customer.list(email=self.get_verified_email())
        # TODO: think of raising exception if there is more than one user with this email in Stripe
        if len(customers_with_email['data']):
            stripe_customer = customers_with_email['data'][0]
        else:
            # else create new Stripe Customer
            stripe_customer = stripe.Customer.create(**self._collect_info_for_stripe_customer())

        self.stripe_id = stripe_customer.stripe_id
        if commit:
            self.save(update_fields=['stripe_id'])

        # sync subscriptions
        Subscription.sync_user_subscription(self)

        return stripe_customer

    def update_stripe_customer(self) -> stripe.Customer:
        """call this method when user is updated"""
        return stripe.Customer.modify(self.stripe_id, **self._collect_info_for_stripe_customer())

    def _collect_info_for_stripe_customer(self) -> dict:
        full_name = self.get_full_name()
        return {
            'name': full_name,
            'description': full_name,
            'email': self.get_verified_email(),
            'metadata': {'id': self.id}
        }

    def get_verified_email(self) -> Union[str, None]:
        try:
            return EmailAddress.objects.get(user=self, verified=True, primary=True).email
        except EmailAddress.DoesNotExist:
            return None

    def get_invitation_name(self):
        if not (invitation_name := str(self.first_name) + ' ' + str(self.last_name)):
            invitation_name = str(self.email).split('@')[0]
        return invitation_name
