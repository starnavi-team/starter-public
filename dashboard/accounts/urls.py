from django.urls import path
from rest_framework import routers
from accounts import views

user_router = routers.DefaultRouter()
user_router.register('me', views.UserViewSet, basename='me')

urlpatterns = [
    path('me/send-invitation/', views.SendInvitationView.as_view(), name='send_invitation'),
]
urlpatterns += user_router.urls
