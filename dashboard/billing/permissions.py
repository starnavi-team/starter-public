from allauth.socialaccount.models import EmailAddress

from rest_framework.permissions import BasePermission

from .models import Subscription


class SubscribePermission(BasePermission):
    message = 'Your profile has not been verified yet or you are already have active subscription.'

    def has_permission(self, request, view):
        if view.action != 'subscribe':
            return True

        # left this query here because it is not related to billing, but for user registration flow
        verified_email_query = EmailAddress.objects.filter(user=request.user, verified=True, primary=True)
        return verified_email_query.exists() and Subscription.user_can_subscribe(request.user)


class SubscribeRetryPermission(BasePermission):
    message = 'You have no subscription in incomplete status.'

    def has_permission(self, request, view):
        if view.action != 'retry':
            return True

        return Subscription.user_can_retry(request.user)


class CancelPermission(BasePermission):
    message = 'You have no subscription to cancel.'

    def has_permission(self, request, view):
        if view.action != 'cancel':
            return True

        return Subscription.user_can_cancel(request.user)
