from django.core.cache import caches
import stripe


def get_stripe_products(limit: int = 100):
    raw_products = []
    has_more = True
    last_product = None
    while has_more:
        products_chunk = stripe.Product.list(limit=limit, starting_after=last_product)
        if not products_chunk:
            break
        raw_products.extend(products_chunk['data'])
        last_product = products_chunk['data'][-1]
        has_more = products_chunk.has_more

    products = {}
    prices = {}
    for product in raw_products:
        for price in stripe.Price.list(product=product.id, limit=100)['data']:
            prices[price.id] = price
        products[product.id] = product

    result = {
        'products': products,
        'prices': prices
    }
    return result


def get_stripe_products_cached():
    cache = caches['stripe-plans-data']
    key_in_cache = 'stripe_plans'

    return cache.get_or_set(key_in_cache, lambda: get_stripe_products())
