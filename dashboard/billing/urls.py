from django.urls import path

from rest_framework.routers import DefaultRouter

from .views import SubscriptionsViewSet, StripeWebhook, PricesView, InvoiceViewSet, SetupIntentsViewSet, \
    PaymentIntentViewSet, SubscriptionEditViewSet, StripeCouponViewSet

router = DefaultRouter()
router.register('subscription', SubscriptionsViewSet, basename='subscription')
router.register('subscription-edit', SubscriptionEditViewSet, basename='subscription_edit')
router.register('invoice', InvoiceViewSet, basename='invoice')
router.register('setup-intents', SetupIntentsViewSet, basename='setup_intents')
router.register('payment-intent', PaymentIntentViewSet, basename='payment_intent')
router.register('coupon', StripeCouponViewSet, basename='coupon')

urlpatterns = router.urls + [
    path('prices/', PricesView.as_view(), name='prices'),
    path('webhook/', StripeWebhook.as_view(), name='webhook'),
]
