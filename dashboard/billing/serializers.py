from django.utils import timezone
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import Serializer, ModelSerializer
from rest_framework.serializers import CharField, DateTimeField, IntegerField, ChoiceField, BooleanField, \
    ListSerializer, EmailField, SerializerMethodField, JSONField, DecimalField
from .models import Subscription
from billing import services
from billing.services import get_stripe_products_cached


# --------------------
# Requests serializers
# --------------------


class SubscribeSerializer(Serializer):
    plan_id = CharField(required=True)
    payment_method_id = CharField(required=True)
    coupon = CharField(default=None, allow_null=True)


class SubscribeRetrySerializer(Serializer):
    payment_method_id = CharField(required=True)


class CancelSerializer(Serializer):
    immediately = BooleanField(required=True)


# ---------------------
# Responses serializers
# --------------------


class StripeTimestampField(DateTimeField):
    def to_representation(self, value):
        if isinstance(value, int):
            return timezone.datetime.fromtimestamp(value, timezone.get_current_timezone())
        return super().to_representation(value)


class StripePaymentStatusSerializer(Serializer):
    status = CharField()
    client_secret = CharField()


class StripePriceRecurringSerializer(Serializer):
    aggregate_usage = CharField()
    interval = CharField()
    interval_count = IntegerField()
    trial_period_days = CharField()
    usage_type = CharField()


class StripePriceSerializer(Serializer):
    id = CharField()
    active = BooleanField()
    billing_scheme = CharField()
    created = StripeTimestampField()
    currency = CharField()
    nickname = CharField()
    product = CharField()
    unit_amount = IntegerField()
    recurring = StripePriceRecurringSerializer(default=None)


class StripeCouponSerializer(Serializer):
    amount_off = IntegerField(default=None)
    # created = StripeTimestampField()
    currency = CharField()
    duration = CharField(default=None)
    duration_in_months = IntegerField(default=None)
    id = CharField()
    # livemode = BooleanField()
    percent_off = DecimalField(max_digits=3, decimal_places=1)
    valid = BooleanField()


class StripeDiscountSerializer(Serializer):
    id = CharField()
    coupon = StripeCouponSerializer(default=None)
    customer = CharField()
    end = StripeTimestampField()
    start = StripeTimestampField()
    subscription = CharField()


class SubscriptionItemsSerializer(Serializer):
    id = CharField()
    price = StripePriceSerializer()


class PendingUpdateStatusSerializer(Serializer):
    expires_at = StripeTimestampField()
    subscription_items = SubscriptionItemsSerializer(many=True)


class StripePlanSerializer(Serializer):
    id = CharField()
    amount = IntegerField(source='unit_amount')
    currency = CharField()
    interval = ChoiceField(choices=['day', 'month', 'year'])
    interval_count = IntegerField()
    description = CharField(source='nickname')

    def to_representation(self, instance):
        instance['interval'] = None if instance['recurring'] is None else instance['recurring']['interval']
        instance['interval_count'] = None if instance['recurring'] is None else instance['recurring']['interval_count']
        return super().to_representation(instance)


class StripeProductSerializer(Serializer):
    id = CharField()
    name = CharField()
    description = CharField()
    images = ListSerializer(child=CharField(), default=[])
    plans = StripePlanSerializer(many=True)


class TransitionsStatusSerializer(Serializer):
    finalized_at = StripeTimestampField()
    marked_uncollectible_at = StripeTimestampField()
    paid_at = StripeTimestampField()
    voided_at = StripeTimestampField()


class DetailedStripeInvoiceSerializer(Serializer):
    number = CharField()

    created = StripeTimestampField()
    due_date = StripeTimestampField()
    status_transitions = TransitionsStatusSerializer()

    plan = SerializerMethodField()
    product = SerializerMethodField()

    discount = StripeDiscountSerializer(default=None)
    currency = CharField()
    total = IntegerField()

    customer_email = EmailField()

    paid = BooleanField()
    status = CharField()
    hosted_invoice_url = CharField()
    invoice_pdf = CharField()

    def get_plan(self, obj):
        plans = []
        for invoice_item in obj.lines['data']:
            if items_plan := invoice_item['price']:
                plan_object = services.get_stripe_products_cached()['prices'].get(items_plan['id'])
                plan_real_name = plan_object.nickname if plan_object else items_plan['id']
                plans.append(plan_real_name)
        return ','.join(plans)

    def get_product(self, obj):
        products = []
        for invoice_item in obj.lines['data']:
            if items_product := invoice_item['plan']:
                product_object = services.get_stripe_products_cached()['products'].get(items_product['product'])
                product_real_name = product_object.name if product_object else items_product['id']
                products.append(product_real_name)
        return ','.join(products)


class StripeSubscriptionSerializer(Serializer):
    status = CharField()

    trial_start = StripeTimestampField()
    trial_end = StripeTimestampField()

    canceled_at = StripeTimestampField()
    ended_at = StripeTimestampField()

    current_period_end = StripeTimestampField()
    days_until_due = StripeTimestampField()
    cancel_at_period_end = BooleanField()

    plan = StripePlanSerializer(source='price')
    discount = StripeDiscountSerializer()

    latest_payment = StripePaymentStatusSerializer(source='latest_invoice.payment_intent', default=None)
    latest_invoice = DetailedStripeInvoiceSerializer()
    pending_update = PendingUpdateStatusSerializer(default=None)

    def to_representation(self, instance):
        instance['price'] = instance['items']['data'][0]['price']
        return super().to_representation(instance)


class SubscriptionSerializer(ModelSerializer):
    details = StripeSubscriptionSerializer(default=None)

    class Meta:
        model = Subscription
        fields = ['details']


class SetupIntentsSerializer(Serializer):
    id = CharField()
    object = CharField()
    application = CharField()
    cancellation_reason = CharField()
    client_secret = CharField()
    created = StripeTimestampField
    customer = CharField()
    description = CharField()
    last_setup_error = CharField()
    livemode = BooleanField()
    mandate = CharField()
    metadata = CharField()
    next_action = CharField()
    on_behalf_of = CharField()
    payment_method = CharField()
    payment_method_options = JSONField()
    payment_method_types = ListSerializer(child=CharField(), default=[])
    single_use_mandate = CharField()
    status = CharField(),
    usage = CharField()


class PaymentIntentSerializer(Serializer):
    id = CharField()
    amount = IntegerField()
    client_secret = CharField()
    last_payment_error = CharField()
    currency = CharField()
    created = StripeTimestampField()


class RequestPaymentIntentsSerializer(Serializer):
    amount = IntegerField(required=True)
    currency = CharField(required=True)


class RequestSubscriptionUpgradeSerializer(Serializer):
    new_price_id = CharField()

    def validate(self, attrs):

        user = self.context['request'].user
        prices = get_stripe_products_cached()['prices']
        new_price_object = prices.get(attrs['new_price_id'])
        if not new_price_object:
            raise ValidationError({'new_price_id': 'Wrong new price data'})
        new_price_amount = new_price_object.get('unit_amount')

        if not (active_subscription := Subscription.user_subscription_to_update(user)):
            raise ValidationError({'subscription': 'There is no subscriptions to update'})

        old_price_object = prices.get(active_subscription.stripe_price_id)
        if not old_price_object:
            raise ValidationError({'new_price_id': 'Wrong new price data'})
        old_price_amount = old_price_object.get('unit_amount')

        if new_price_amount < old_price_amount:
            raise ValidationError({'new_price': 'Subscription downgrade is not available'})
        return attrs
