import logging
import stripe
from stripe.error import SignatureVerificationError, InvalidRequestError
from drf_yasg.utils import swagger_auto_schema
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.settings import api_settings
from rest_framework import viewsets
from rest_framework.mixins import ListModelMixin
from rest_framework.decorators import action
from rest_framework import status
from datetime import datetime, timezone
from billing import constants
from .permissions import SubscribePermission, SubscribeRetryPermission, CancelPermission
from .serializers import SubscriptionSerializer, SubscribeSerializer, SubscribeRetrySerializer, CancelSerializer, \
    StripeProductSerializer, DetailedStripeInvoiceSerializer, SetupIntentsSerializer, RequestPaymentIntentsSerializer, \
    PaymentIntentSerializer, RequestSubscriptionUpgradeSerializer, StripeCouponSerializer
from .models import Subscription
from drf_yasg import openapi

logger = logging.getLogger(__name__)


class BaseSubscriptionsViewSet(viewsets.GenericViewSet):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES + [
        SubscribePermission,
        SubscribeRetryPermission,
        CancelPermission,
    ]
    serializer_class = SubscriptionSerializer

    filter_backends = []
    pagination_class = None

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # queryset just for schema generation metadata
            return Subscription.objects.all()
        return Subscription.objects.filter(user=self.request.user)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())

        try:
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            obj = None

        if obj is not None:
            self.check_object_permissions(self.request, obj)

        return obj


class SubscriptionsViewSet(ListModelMixin, BaseSubscriptionsViewSet):

    @swagger_auto_schema(
        operation_summary='Get user subscription details',
        operation_description='Get user subscription details',
        responses={status.HTTP_200_OK: SubscriptionSerializer}
    )
    def list(self, request, *args, **kwargs):
        """This one actually returns only the one user subscription"""
        subscription = self.get_object()
        serializer = self.get_serializer(instance=subscription)
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_summary='Subscribe',
        operation_description='Subscribe to Plan plan_id with payment method payment_method_id',
        request_body=SubscribeSerializer,
        responses={status.HTTP_200_OK: SubscriptionSerializer}
    )
    @action(methods=['POST'], detail=False)
    def subscribe(self, request):
        request.user.sync_stripe_customer(True)

        serializer = SubscribeSerializer(data=request.data)
        serializer.is_valid(True)
        data = serializer.validated_data

        subscription, _ = Subscription.subscribe(
            request.user, data['plan_id'], data['payment_method_id'],
            coupon=data['coupon']
        )
        response = SubscriptionSerializer(subscription).data
        return Response(response)

    @swagger_auto_schema(
        operation_summary='Retry subscription payment',
        operation_description='Retry initial payment for subscription with new payment method',
        request_body=SubscribeRetrySerializer,
        responses={status.HTTP_200_OK: SubscriptionSerializer}
    )
    @action(methods=['POST'], detail=False)
    def retry(self, request):
        serializer = SubscribeRetrySerializer(data=request.data)
        serializer.is_valid(True)

        subscription = self.get_object()
        subscription.retry_subscribing(serializer.validated_data['payment_method_id'])

        response = self.get_serializer(instance=subscription).data
        return Response(response)

    @swagger_auto_schema(
        operation_summary='Cancel the subscription',
        operation_description='Cancel the subscription (immediately or at the period end)',
        request_body=CancelSerializer,
        responses={status.HTTP_200_OK: SubscriptionSerializer}
    )
    @action(methods=['POST'], detail=False)
    def cancel(self, request):
        serializer = CancelSerializer(data=request.data)
        serializer.is_valid(True)

        subscription = self.get_object()
        subscription.cancel(serializer.validated_data['immediately'])

        response = self.get_serializer(instance=subscription).data
        return Response(response)


class SubscriptionEditViewSet(BaseSubscriptionsViewSet):

    def get_serializer_class(self, request_serializer=None):
        if request_serializer:
            return RequestSubscriptionUpgradeSerializer
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        if request_serializer := kwargs.get('request_serializer'):
            kwargs.pop('request_serializer')
        serializer_class = self.get_serializer_class(request_serializer=request_serializer)
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    @swagger_auto_schema(
        operation_summary='Upgrade subscription price',
        operation_description='User can upgrade his subscription and pay a new price',
        request_body=RequestSubscriptionUpgradeSerializer,
        responses={status.HTTP_200_OK: SubscriptionSerializer}
    )
    @action(methods=['POST'], detail=False)
    def upgrade(self, request):

        request_serializer = self.get_serializer(
            request_serializer=True,
            data=request.data,
        )
        request_serializer.is_valid(raise_exception=True)

        subscription: Subscription = self.get_object()

        stripe_subscription = stripe.Subscription.retrieve(subscription.stripe_id)

        stripe.Subscription.modify(
            stripe_subscription.id,
            payment_behavior='pending_if_incomplete',
            proration_behavior='always_invoice',
            expand=['latest_invoice', 'latest_invoice.payment_intent'],
            items=[{
                'id': stripe_subscription['items']['data'][0].id,
                'price': request_serializer.data['new_price_id'],
            }]
        )
        response = self.get_serializer(instance=subscription).data
        return Response(response)

    @swagger_auto_schema(
        operation_summary='Get prorations',
        operation_description='Get prorations before update a subscription',
        request_body=RequestSubscriptionUpgradeSerializer,
        responses={
            status.HTTP_200_OK:
                openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'current_prorations': openapi.Schema(type=openapi.TYPE_INTEGER)
                    }
                )
        })
    @action(methods=['POST'], detail=False, url_path='get-prorations', url_name='get_prorations')
    def get_prorations(self, request):
        request_serializer = self.get_serializer(
            request_serializer=True,
            data=request.data,
        )
        request_serializer.is_valid(raise_exception=True)

        subscription: Subscription = self.get_object()

        stripe_subscription = stripe.Subscription.retrieve(subscription.stripe_id)
        items = [{
            'id': stripe_subscription['items']['data'][0].id,
            'price': request_serializer.data['new_price_id'],
        }]

        proration_date = int(datetime.utcnow().replace(tzinfo=timezone.utc).timestamp())

        invoice = stripe.Invoice.upcoming(
            customer=self.request.user.stripe_id,
            subscription=stripe_subscription.id,
            subscription_items=items,
            subscription_proration_date=proration_date,
        )
        invoice_items = [ii for ii in invoice.lines.data if ii.period.start - proration_date <= 1]
        cost = sum([p.amount for p in invoice_items])
        return Response({'current_prorations': cost})


class StripeCouponViewSet(viewsets.ViewSet):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES

    @swagger_auto_schema(
        operation_summary='Get coupon data by coupon_id',
        operation_description='Returns coupon data summary',
        manual_parameters=[
            openapi.Parameter(
                'coupon_id',
                openapi.IN_QUERY,
                description="coupon id to search for",
                type=openapi.TYPE_STRING,
                required=True
            )
        ],
        responses={status.HTTP_200_OK: StripeCouponSerializer}
    )
    def list(self, request, *args, **kwargs):

        coupon_id = request.query_params.get('coupon_id')
        if not coupon_id:
            raise ValidationError({'coupon_id': 'coupon_id field is a mandatory parameter'})
        try:
            coupon = stripe.Coupon.retrieve(coupon_id)
        except InvalidRequestError as e:
            raise ValidationError({'coupon_id': e.user_message})

        serializer = StripeCouponSerializer(instance=coupon)
        return Response(serializer.data)


class InvoiceViewSet(viewsets.ViewSet):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES

    @swagger_auto_schema(
        operation_summary='Customers invoices list endpoint',
        operation_description='Returns list of all invoices for current customer',
        responses={status.HTTP_200_OK: DetailedStripeInvoiceSerializer(many=True)}
    )
    def list(self, request):
        customer_id = request.user.stripe_id
        if not customer_id:
            return Response([])

        invoices = []
        has_more = True
        last_invoice = None
        while has_more:
            invoices_chunk = stripe.Invoice.list(customer=customer_id, limit=100, starting_after=last_invoice)
            if not invoices_chunk:
                break
            invoices.extend(invoices_chunk['data'])
            last_invoice = invoices_chunk['data'][-1]
            has_more = invoices_chunk.has_more
        serializer = DetailedStripeInvoiceSerializer(instance=invoices, many=True)
        return Response(serializer.data)


class SetupIntentsViewSet(viewsets.ViewSet):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES

    @swagger_auto_schema(
        operation_summary='SetupIntents endpoint',
        operation_description='Get Stripe SetupIntents',
        responses={status.HTTP_200_OK: SetupIntentsSerializer()}
    )
    def list(self, request):
        customer_id = request.user.stripe_id
        if not customer_id:
            raise ValidationError(
                {'stripe_id': 'There was an error with your account. Please, contact our user support.'}
            )
        intents = stripe.SetupIntent.create()
        serializer = SetupIntentsSerializer(instance=intents)
        return Response(serializer.data)


class PaymentIntentViewSet(viewsets.ViewSet):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES

    @swagger_auto_schema(
        operation_summary='PaymentIntents endpoint',
        operation_description='Returns stripe PaymentIntents',
        request_body=RequestPaymentIntentsSerializer(),
        responses={status.HTTP_200_OK: PaymentIntentSerializer()}
    )
    def create(self, request):
        customer_id = request.user.stripe_id
        if not customer_id:
            raise ValidationError(
                {'stripe_id': 'There was an error with your account. Please, contact our user support.'}
            )

        payment_initial_data_serializer = RequestPaymentIntentsSerializer(data=request.data)
        payment_initial_data_serializer.is_valid(raise_exception=True)
        payment_intent = stripe.PaymentIntent.create(
            amount=payment_initial_data_serializer.data['amount'],
            currency=payment_initial_data_serializer.data['currency'],
        )
        serializer = PaymentIntentSerializer(instance=payment_intent)

        return Response(serializer.data)


class PricesView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_summary='Prices endpoint',
        operation_description='Returns all the active services with all their active plans',
        responses={status.HTTP_200_OK: StripeProductSerializer(many=True)}
    )
    def get(self, request):
        data = Subscription.get_prices()
        serializer = StripeProductSerializer(instance=data, many=True)
        return Response(serializer.data)


class StripeWebhook(APIView):
    permission_classes = [AllowAny]

    @classmethod
    def verify_request(cls, request):
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']

        try:
            event = stripe.Webhook.construct_event(payload, sig_header, settings.STRIPE_WEBHOOK_KEY)
        except ValueError:
            logger.warning('Got invalid payload from Stripe webhook')
            raise ValidationError
        except SignatureVerificationError:
            logger.warning('got invalid signature from Stripe webhook')
            raise ValidationError

        return event

    @classmethod
    def handle_subscription_updated(cls, event):
        stripe_subscription = event.data.object
        try:
            subscription = Subscription.objects.get(stripe_id=stripe_subscription.id)
        except Subscription.DoesNotExist:
            logger.warning(
                f'Got event from Stripe about subscription that doesn''t exist locally. '
                f'ID = {stripe_subscription.id}'
            )
            raise ValidationError

        # For now remembering only subscription status. Other info should be get using SubscriptionView
        for field, value in Subscription.extract_info_from_stripe(stripe_subscription).items():
            setattr(subscription, field, value)
        subscription.save()

    @classmethod
    def handle_subscription_deleted(cls, event):
        stripe_subscription = event.data.object
        try:
            subscription = Subscription.objects.get(stripe_id=stripe_subscription.id)
        except Subscription.DoesNotExist:
            logger.warning(
                f'Got event from Stripe about subscription that doesn''t exist locally. '
                f'ID = {stripe_subscription.id}'
            )
            return

        for field, value in Subscription.extract_info_from_stripe(stripe_subscription).items():
            setattr(subscription, field, value)
        subscription.save()

    @swagger_auto_schema(
        operation_summary='Stripe subscription webhook',
        operation_description='See docs at https://stripe.com/docs/api/events/object',
        responses={status.HTTP_200_OK: ''}
    )
    def post(self, request):
        event = self.verify_request(request)

        # Handle the event
        handler = getattr(self, constants.STRIPE_WEB_HOOKS[event.type], None)
        if handler:
            handler(event)
        else:
            logger.warning(f'Got unexpected event type on Stripe webhook: {event.type}')
            raise ValidationError

        return Response(status=200)
