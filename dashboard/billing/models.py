from logging import getLogger
from itertools import chain
from typing import Tuple, List, Union

import stripe
from stripe.error import CardError, InvalidRequestError

from rest_framework.exceptions import ValidationError

from django.contrib.auth import get_user_model
from django.db import models

logger = getLogger(__name__)
User = get_user_model()


class SubscriptionStatus(models.TextChoices):
    """Got these from https://stripe.com/docs/api/subscriptions/object#subscription_object-status"""
    # the user started subscribing, but must also pass 3D Secure or something was wrong with the charge
    incomplete = 'incomplete'
    # the user started subscribing, the initial charge failed and the user didn't pay in 23 hours
    incomplete_expired = 'incomplete_expired'
    trialing = 'trialing'
    active = 'active'
    past_due = 'past_due'
    canceled = 'canceled'
    unpaid = 'unpaid'


SUBSCRIPTION_ACTIVE_STATUSES = [
    SubscriptionStatus.trialing.value,
    SubscriptionStatus.active.value,
    SubscriptionStatus.past_due.value,
    SubscriptionStatus.incomplete.value,
]

SUBSCRIPTION_READY_TO_UPDATE_STATUSES = [
    SubscriptionStatus.trialing.value,
    SubscriptionStatus.active.value,
]

SUBSCRIPTION_FINAL_STATUSES = [
    SubscriptionStatus.canceled.value,
    SubscriptionStatus.incomplete_expired.value,
    SubscriptionStatus.unpaid.value,
]

SUBSCRIPTION_NEED_CANCELLATION_STATUSES = [
    SubscriptionStatus.trialing.value,
    SubscriptionStatus.active.value,
    SubscriptionStatus.past_due.value,
    SubscriptionStatus.incomplete.value,
]


class Subscription(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name='subscriptions',
        verbose_name='User',
        db_index=True,  # lookups by user
    )

    stripe_id = models.CharField('Stripe ID', max_length=32)
    stripe_product_id = models.CharField('Stripe product ID', max_length=32)
    stripe_price_id = models.CharField('Stripe price ID', max_length=32)

    status = models.CharField('Status', max_length=18, choices=SubscriptionStatus.choices)

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'subscriptions'
        ordering = ['-id']
        constraints = [
            models.constraints.UniqueConstraint(fields=['user'], name='unique_user_subscription')
        ]

    def get_detail_info(self, expand=('latest_invoice.payment_intent',)) -> stripe.Subscription:
        return stripe.Subscription.retrieve(self.stripe_id, expand=expand)

    @property
    def details(self):
        x = self.get_detail_info()
        return x

    def get_billing_history(self, starting_after=None) -> dict:
        return stripe.Invoice.list(subscription=self.stripe_id, starting_after=starting_after)

    # Billing flow methods below
    # We're catching only CardError because it is user related.
    # All other exceptions should be treated as internal (500 status code)

    @classmethod
    def user_can_subscribe(cls, user: User):
        return not cls.objects.filter(~models.Q(status__in=SUBSCRIPTION_FINAL_STATUSES), user=user).exists()

    @classmethod
    def user_can_retry(cls, user: User):
        return cls.objects.filter(user=user, status=SubscriptionStatus.incomplete.value).exists()

    @classmethod
    def user_can_cancel(cls, user: User):
        return cls.objects.filter(user=user, status__in=SUBSCRIPTION_ACTIVE_STATUSES).exists()

    @classmethod
    def user_subscription_to_update(cls, user: User):
        return cls.objects.filter(user=user, status__in=SUBSCRIPTION_READY_TO_UPDATE_STATUSES).first()

    @classmethod
    def subscribe(
            cls,
            user: User,
            plan_id: str,
            payment_method_id: str,
            coupon: str = None
    ) -> Tuple['Subscription', stripe.Subscription]:

        try:
            # sid =
            stripe.PaymentMethod.attach(payment_method_id, customer=user.stripe_id)  # noqa
            stripe.Customer.modify(user.stripe_id, invoice_settings={'default_payment_method': payment_method_id})
        except CardError:
            raise ValidationError('Something is wrong with your card credentials, please provide correct ones.')

        # if stripe fails then there will be no local record for subscription - correct
        try:
            stripe_subscription = stripe.Subscription.create(
                customer=user.stripe_id,
                items=[{'price': plan_id}],
                coupon=coupon,
                expand=['latest_invoice', 'latest_invoice.payment_intent'],
            )
        except CardError as exc:
            raise ValidationError(f'Charging error: {exc.user_message}')
        # if front sends wrong coupon we will have InvalidRequestError
        except InvalidRequestError as exc:
            raise ValidationError(f'Request error: {exc.user_message}')

        try:
            # trying to update local subscription record first
            subscription = cls.objects.get(user=user)
            for field, value in cls.extract_info_from_stripe(stripe_subscription).items():
                setattr(subscription, field, value)
            subscription.save()
        except cls.DoesNotExist:
            # if it doesn't exist - create a new one
            subscription = cls.create_from_stripe(stripe_subscription, user)

        return subscription, stripe_subscription

    def retry_subscribing(self, payment_method_id: str) -> stripe.Subscription:
        # At this time the Subscription exists and Stripe Customer for the user exists too
        customer_id = self.user.stripe_id  # noqa

        # Trying to attach new card. If it is wrong – raise an Error
        try:
            stripe.PaymentMethod.attach(payment_method_id, customer=customer_id)  # noqa
            stripe.Customer.modify(customer_id, invoice_settings={'default_payment_method': payment_method_id})
        except CardError:
            raise ValidationError('Something is wrong with your card credentials, please provide correct ones.')

        # Try to charge user again. If error happened - it will be got with next subscription retrieval (latest_invoice)
        latest_invoice_id = self.get_detail_info(['latest_invoice.payment_intent']).latest_invoice.id
        try:
            stripe.Invoice.pay(latest_invoice_id, payment_method=payment_method_id)
        except CardError:
            pass

        stripe_subscription = self.details
        # if status has changed - we should update local record immediately in order to let user use services
        if stripe_subscription['status'] != self.status:
            self.status = stripe_subscription['status']
            self.save(update_fields=['status'])

        return stripe_subscription

    def cancel(self, immediately: bool) -> stripe.Subscription:
        if immediately:
            stripe_subscription = stripe.Subscription.delete(sid=self.stripe_id)
        else:
            stripe_subscription = stripe.Subscription.modify(self.stripe_id, cancel_at_period_end=True)

        if stripe_subscription['status'] != self.status:
            self.status = stripe_subscription['status']
            self.save(update_fields=['status'])

        return stripe_subscription

    @classmethod
    def sync_user_subscription(cls, user: User) -> Union['Subscription', None]:
        """This function will synchronize (and create on absent) local subscription record with the Stripe one"""
        stripe_subscriptions = [
            stripe.Subscription.auto_paging_iter(customer=user.stripe_id, status=status)
            for status in SUBSCRIPTION_ACTIVE_STATUSES
        ]
        # get all the user's active subscriptions from Stripe
        stripe_subscriptions = list(chain(*stripe_subscriptions))

        # if there is more than one active subscription in Stripe - something went wrong in billing, raise an error
        if len(stripe_subscriptions) > 1:
            err = f'User with email = {user.email} have multiple active subscriptions in Stripe'
            logger.warning(err)
            raise RuntimeError(err)

        # if user has no active subscriptions in Stripe - clean local active record
        if len(stripe_subscriptions) == 0:
            Subscription.objects.filter(~models.Q(status__in=SUBSCRIPTION_FINAL_STATUSES), user=user).delete()
            return None

        # now user definitely has only one active Stripe subscription
        stripe_subscription = stripe_subscriptions[0]

        # synchronize local subscription record with the only active in Stripe
        try:
            subscription = cls.objects.get(user=user)
            # local record exists - sync its fields
            for field, value in cls.extract_info_from_stripe(stripe_subscription).items():
                setattr(subscription, field, value)
            subscription.save()
        except cls.DoesNotExist:
            # local record doesn't exist - create it
            subscription = cls.create_from_stripe(stripe_subscription, user)

        return subscription

    @classmethod
    def create_from_stripe(cls, stripe_subscription: stripe.Subscription, user: User) -> 'Subscription':
        return cls.objects.create(user=user, **cls.extract_info_from_stripe(stripe_subscription))

    @classmethod
    def extract_info_from_stripe(cls, stripe_subscription: stripe.Subscription) -> dict:
        return {
            'stripe_id': stripe_subscription.id,
            'stripe_product_id': stripe_subscription['items']['data'][0].price.product,
            'stripe_price_id': stripe_subscription['items']['data'][0].price.id,
            'status': stripe_subscription.status,
        }

    @classmethod
    def get_prices(cls) -> List[stripe.Product]:
        """For every Product record there would be also plans array of stripe.Price"""
        products = stripe.Product.list(active='true')['data']
        for product in products:
            product['plans'] = stripe.Price.list(product=product.id, active='true')['data']
        return products
