STRIPE_WEB_HOOKS = {
    'customer.subscription.updated': 'handle_subscription_updated',
    'customer.subscription.deleted': 'handle_subscription_deleted'
}
