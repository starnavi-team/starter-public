import base64
from tests.base import BaseAPITestCaseSetup
from rest_framework.reverse import reverse
from rest_framework import status
from django.core import mail
from tests.base import get_activation_info
from tests.factories import factories
from allauth.account import models as allauth_models


class SendInvitationTestCase(BaseAPITestCaseSetup):

    def test_send_invitation_success(self):
        payload = {'invited_email': 'ddsadasd@sad578873-sedddd.com'}

        response = self.client.post(reverse('send_invitation'), payload)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(reverse('send_invitation'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        encoded_mail = get_activation_info(mail.outbox[0].body).split(':')[1]
        decoded_mail = base64.urlsafe_b64decode(encoded_mail.encode('ascii') + 3 * b'=').decode('ascii')

        self.assertEqual(decoded_mail, payload['invited_email'])

    def test_send_invitation_fail_no_invited_email(self):
        payload = {}

        response = self.client.post(reverse('send_invitation'), payload)
        self.assertContains(response, 'This field is required', status_code=status.HTTP_400_BAD_REQUEST)

    def test_send_invitation_fail_wrong_invited_email(self):
        payload = {
            'invited_email': 'ddsadasd@sad578873_sedddd.com'
        }

        response = self.client.post(reverse('send_invitation'), payload)
        self.assertContains(response, 'Enter a valid email address', status_code=status.HTTP_400_BAD_REQUEST)

    def test_send_invitation_fail_anonymous_user(self):
        payload = {'invited_email': 'ddsadasd@sad578873-sedddd.com'}

        self.client.logout()
        response = self.client.post(reverse('send_invitation'), payload)
        self.assertContains(response, 'Authentication credentials were not provided',
                            status_code=status.HTTP_401_UNAUTHORIZED)

    def test_send_invitation_fail_user_already_exists(self):
        self.user = factories.UserFactory(
            email='ddsadasd@sad578873-sedddd.com', password='test12345',
        )
        allauth_models.EmailAddress.objects.create(
            user=self.user,
            email=self.user.email,
            verified=True,
            primary=True,
        )

        payload = {'invited_email': 'ddsadasd@sad578873-sedddd.com'}
        response = self.client.post(reverse('send_invitation'), payload)

        self.assertContains(response, 'User with this email is already our customer',
                            status_code=status.HTTP_400_BAD_REQUEST)
