from tests.base import BaseAPITestCaseSetup
from accounts import models
from rest_framework.reverse import reverse
from rest_framework import status
from billing import models as billing_models
from random import choice


class UserRemoveHisAccountTest(BaseAPITestCaseSetup):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        billing_models.Subscription.objects.all().delete()

    def create_subscription(self, **overrides):
        data = {
            'user': self.user,
            'stripe_id': 'stripe_id',
            'stripe_product_id': 'stripe_product_id',
            'stripe_price_id': 'stripe_price_id',
            'status': billing_models.SubscriptionStatus.active.value
        }
        data.update(overrides)
        subscription = billing_models.Subscription.objects.create(**data)
        return subscription

    def test_user_remove_his_account_fail_user_have_active_subscriptions(self):
        self.create_subscription(
            user=self.user,
            stripe_id='first_id',
            stripe_product_id='First_product_id',
            stripe_price_id='First_price_id',
            status=choice(billing_models.SUBSCRIPTION_NEED_CANCELLATION_STATUSES)
        )
        # For now user can have one subscription only. So when it will change we need to test with more then one
        # subscriptions
        #
        # second_subscription = self.create_subscription(
        #     user=self.user,
        #     stripe_id='second_id',
        #     stripe_product_id='second_product_id',
        #     stripe_price_id='second_price_id',
        #     status=choice(billing_models.SUBSCRIPTION_NEED_CANCELLATION_STATUSES)
        # )

        response = self.client.post(reverse('me-delete_my_account'), {'password': self.post_data['password1']})
        self.assertContains(response, "You have active subscriptions", status_code=status.HTTP_400_BAD_REQUEST)

    def test_user_remove_his_account_success(self):
        before_subscriptions_create = len(billing_models.Subscription.objects.all())

        # there is no subscription
        self.assertEqual(before_subscriptions_create, 0)

        self.create_subscription(
            user=self.user,
            stripe_id='first_id',
            stripe_product_id='First_product_id',
            stripe_price_id='First_price_id',
            status=billing_models.SubscriptionStatus.canceled.value
        )

        # there is one subscription in the DB
        after_subscriptions_create = len(billing_models.Subscription.objects.all())
        self.assertEqual(after_subscriptions_create, 1)

        # current user id
        current_user_id = self.user.id
        user_from_db_before_deletion = models.User.objects.filter(id=current_user_id).first()
        self.assertEqual(self.user, user_from_db_before_deletion)

        # send request to delete user
        response = self.client.post(reverse('me-delete_my_account'), {'password': self.post_data['password1']})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # try to get user by id
        user_from_db_after_deletion = models.User.objects.filter(id=current_user_id).first()
        self.assertEqual(user_from_db_after_deletion, None)

        # get number of subscriptions
        subscriptions_num_after_deletion = len(billing_models.Subscription.objects.all())
        self.assertEqual(subscriptions_num_after_deletion, 0)

    def test_user_remove_his_account_fail_wrong_password(self):
        # send request to delete user
        response = self.client.post(reverse('me-delete_my_account'), {'password': 'dasdsadasdasdasdas'})
        self.assertContains(response, 'Your password was entered incorrectly. Please enter it again',
                            status_code=status.HTTP_400_BAD_REQUEST)

    def test_user_remove_his_account_fail_anonymous_user(self):
        self.client.logout()
        response = self.client.post(reverse('me-delete_my_account'))
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
