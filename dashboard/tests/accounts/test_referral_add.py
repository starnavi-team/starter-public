from tests.base import BaseAPITestCaseSetup
from unittest.mock import patch
from accounts import models
from rest_framework.reverse import reverse
from rest_framework import status
from tests.factories import factories


class ReferralObjectAddTestCase(BaseAPITestCaseSetup):

    def test_referral_add_only_once_success(self):
        self.user = factories.UserFactory(
            email='test_user2@example.com', password='test12345',
        )
        referral = self.user.own_referral

        self.user.first_name = 'FIRST NAME'
        self.user.save()
        self.assertEqual(referral, self.user.own_referral)

    def test_referral_fail_not_unique(self):
        patched_gen = patch('accounts.services.generate_referral_code', return_value='12345')

        with patched_gen, self.assertRaises(ValueError):
            self.user = factories.UserFactory(
                email='test_user2@example.com', password='test12345',
            )
            self.user = factories.UserFactory(
                email='test_user3@example.com', password='test12345',
            )


class ReferralAddByViewTestCase(BaseAPITestCaseSetup):

    def test_user_add_with_referral_success(self):
        post_data = {
            'email': 'test_user25@example.com',
            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
            'invited_with': self.user.own_referral
        }

        response = self.client.post(reverse('rest_register'), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        new_user = models.User.objects.filter(email=post_data['email']).first()
        self.assertEqual(new_user.invited_by, self.user)

    def test_user_add_with_referral_success_but_with_wrong_referral(self):
        post_data = {
            'email': 'test_user25@example.com',
            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
            'invited_with': self.user.own_referral + 'asdlkjhaskdyhajk'
        }

        response = self.client.post(reverse('rest_register'), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        new_user = models.User.objects.filter(email=post_data['email']).first()
        self.assertEqual(new_user.invited_by, None)


class UserGetHisReferralCodeTestCase(BaseAPITestCaseSetup):

    def test_get_his_referral_code_success(self):
        response = self.client.get(reverse('me-list'))
        self.assertNotEqual(self.user.own_referral, None)
        self.assertEqual(response.json()['own_referral'], self.user.own_referral)


class UserWithMultipleReferredUsersViewTestCase(BaseAPITestCaseSetup):

    def test_delete_user_success(self):
        mails = ['test_user{}25@example.com'.format(i) for i in range(4)]
        post_data = {

            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
            'invited_with': self.user.own_referral
        }
        # invite many users
        for _mail in mails:
            post_data['email'] = _mail
            response = self.client.post(reverse('rest_register'), post_data)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        overall_invited = models.User.objects.filter(invited_by=self.user).values_list('id', flat=True)
        self.assertEqual(len(overall_invited), len(mails))

        # send request to delete user
        response = self.client.post(reverse('me-delete_my_account'), {'password': self.post_data['password1']})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # test if cleaned invited_by after inviter was deleted
        previously_invited = models.User.objects.filter(id__in=overall_invited)
        for _user in previously_invited:
            self.assertEqual(_user.invited_by, None)
