""" Tests for accounts views."""
from rest_framework import status
from rest_framework.reverse import reverse
from tests.base import BaseAPITestCaseSetup


class UserRegistrationTest(BaseAPITestCaseSetup):
    """ Tests for User registration."""

    def setUp(self):
        super().setUp()

        self.post_data = {
            'email': 'new_test_user@example22.com',
            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

    def test_user_registration_with_invalid_data(self):
        """Test registration with invalid data."""

        response = self.client.post(reverse('rest_register'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_registration_with_valid_data(self):
        """Test registration with valid data."""

        response = self.client.post(reverse('rest_register'), self.post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UserLoginTest(BaseAPITestCaseSetup):
    """ Tests for User login."""

    def setUp(self):
        super().setUp()

        self.user_credentials = {
            'email': 'test_user@example.com',
            'password': 'test12345',
        }

    def test_user_login_with_invalid_data(self):
        """Test login with invalid data."""

        response = self.client.post(reverse('rest_login'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_login_with_valid_data(self):
        """Test login with valid data."""

        response = self.client.post(reverse('rest_login'), self.user_credentials)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserLogoutTest(BaseAPITestCaseSetup):
    """ Tests for User logout."""

    def test_user_logout(self):
        """Test logout."""
        response = self.client.get(
            reverse('api-root', ),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(reverse('rest_logout'), {})
        self.client.credentials()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(
            reverse('api-root', ),
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestUserMeAPI(BaseAPITestCaseSetup):

    def test_get_my_data(self):
        url = reverse('me-list')
        response = self.client.get(url)

        user_id = self.user.id
        self.assertEqual(response.data['id'], user_id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_my_data(self):
        user_id = self.user.id
        url = reverse('me-detail', kwargs={'pk': user_id})

        payload = {
            'first_name': 'update_first_name',
            'last_name': 'update_last_name',

        }
        response = self.client.patch(url, payload)
        self.assertEqual(response.data['id'], user_id)
        self.assertEqual(response.data['first_name'], 'update_first_name')
        self.assertEqual(response.data['last_name'], 'update_last_name')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_my_data_ignore_email(self):
        user_id = self.user.id
        user_email = self.user.email
        url = reverse('me-detail', kwargs={'pk': user_id})
        payload = {
            'email': 'update_email@email.com'

        }
        response = self.client.patch(url, payload)
        self.assertEqual(response.data['id'], user_id)
        self.assertEqual(response.data['email'], user_email)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
