from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from allauth.socialaccount.models import SocialApp
from allauth.socialaccount.providers.facebook import provider as facebook_provider
from django.contrib.sites.models import Site
from rest_framework import status
import responses


class RegistrationWithSocialAccountTestCase(APITestCase):
    def setUp(self):
        social_app = 'Facebook_Social_APP'
        provider = 'facebook'
        facebook_id = '978288742545006'
        facebook_secret = 'ee4c186c5ff5b5f60908d4b3a56c0ab3'
        self.initial_facebook_post = {
            "access_token":
                "EAAN5v5j8tm4BABjIZAuZAQBnIIxgxyZAFOeDetBSokt7ucJ7TtXqmbmhG2ZBZAYyZChSPeVZ"
                "BFSunrNxUyIY2O0gfAjUYFUpDZC6b7V4EaHPj7qsvpH5ONZCVJ4hAq5v5QzYYGjvARWDh3mo0K"
                "0qm1Hoksw95g3zRhAS7ZCUMAlgw97Q9DlaiLGLwDITNLnWo6gY3xiR8hU5SYAQZDZD"
        }
        self.test_site = 'new_site.com'
        self.facebook_full_response = {
            'email': 'some_email@mail.com',
            'id': '345723498237',
            'name': 'Some Name',
            'picture': {}
        }

        site = Site.objects.all().first()

        self.social_app = SocialApp.objects.create(
            provider=provider,
            name=social_app,
            client_id=facebook_id,
            secret=facebook_secret,
        )

        self.social_app.sites.add(site)

    @responses.activate
    def test_registration_with_social_account(self):
        facebook_url = facebook_provider.GRAPH_API_URL + '/me'
        responses.add(responses.GET, facebook_url, json=self.facebook_full_response, status=200)
        payload = self.initial_facebook_post
        response = self.client.post(reverse('fb_login'), payload)
        self.assertContains(response, 'E-mail is not verified', status_code=status.HTTP_400_BAD_REQUEST)

    @responses.activate
    def test_registration_without_email_with_social_account(self):
        facebook_response = self.facebook_full_response.copy()
        facebook_response['email'] = ''
        facebook_url = facebook_provider.GRAPH_API_URL + '/me'
        responses.add(responses.GET, facebook_url, json=facebook_response, status=200)
        payload = self.initial_facebook_post
        response = self.client.post(reverse('fb_login'), payload)
        self.assertContains(response, 'E-mail is required. Please add an e-mail to your social account',
                            status_code=status.HTTP_400_BAD_REQUEST)
