import factory
from django.contrib.auth import get_user_model


class UserFactory(factory.DjangoModelFactory):
    """Factory for User."""

    class Meta:
        model = get_user_model()
        django_get_or_create = ('email',)

    email = factory.Faker('email')
    password = factory.PostGenerationMethodCall(
        'set_password',
        'default_password',
    )
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
