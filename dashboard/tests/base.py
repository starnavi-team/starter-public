from django.test import override_settings
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from tests.factories import factories
from allauth.account.models import EmailAddress


def get_activation_info(body_string=None):
    try:
        url_start_char_position = body_string.index('http')
    except ValueError:
        return None
    url_end_char_position = body_string.find('\n', url_start_char_position)
    parts = body_string[url_start_char_position: url_end_char_position].rstrip('/').split('/')

    return parts[-1]


@override_settings(CACHE_BACKEND='django.core.cache.backends.dummy.DummyCache')
class BaseAPITestCaseSetup(APITestCase):
    def setUp(self):
        self.user = factories.UserFactory(
            email='test_user@example.com', password='test12345',
        )
        EmailAddress.objects.create(
            user=self.user,
            email=self.user.email,
            verified=True,
            primary=True,
        )
        self.post_data = {
            'email': 'test_user@example.com',
            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }
        response = self.client.post(reverse('rest_login'), {'email': 'test_user@example.com', 'password': 'test12345'})
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {response.data["access_token"]}')
