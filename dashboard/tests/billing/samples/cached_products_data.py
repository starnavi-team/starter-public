cached_product_data = {
    'products':
        {
            'prod_HhTXEEEFNDxLOa':
                {
                    "active": True,
                    "attributes": [],
                    "created": 1595511326,
                    "description": "This is for enterprise solutions, all the batteries included",
                    "id": "prod_HhTXEEEFNDxLOa",
                    "images": [],
                    "livemode": False,
                    "metadata": {},
                    "name": "Enterprise subscription",
                    "object": "product",
                    "statement_descriptor": None,
                    "type": "service",
                    "unit_label": None,
                    "updated": 1596105069
                },
            'prod_HhOWSzt9IRCfjT':
                {
                    "active": True,
                    "attributes": [],
                    "created": 1595492713,
                    "description": "This is for personal usage",
                    "id": "prod_HhOWSzt9IRCfjT",
                    "images": [],
                    "livemode": False,
                    "metadata": {},
                    "name": "Personal subscription",
                    "object": "product",
                    "statement_descriptor": None,
                    "type": "service",
                    "unit_label": None,
                    "updated": 1595511476
                }
        },
    'prices':
        {
            'price_1H84a3KAn97x2zX0WM3W3j1z':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511327,
                    "currency": "usd",
                    "id": "price_1H84a3KAn97x2zX0WM3W3j1z",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Yearly subscription \u2013\u00a0most efficient plan",
                    "object": "price",
                    "product": "prod_HhTXEEEFNDxLOa",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "year",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 100000,
                    "unit_amount_decimal": "100000"
                },
            'price_1H84a3KAn97x2zX0xCStUi28':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511327,
                    "currency": "usd",
                    "id": "price_1H84a3KAn97x2zX0xCStUi28",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Monthly plan \u2013\u00a0for short-term projects",
                    "object": "price",
                    "product": "prod_HhTXEEEFNDxLOa",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "month",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 10000,
                    "unit_amount_decimal": "10000"
                },
            'price_1H84a3KAn97x2zX0rOkE1bTz':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511327,
                    "currency": "usd",
                    "id": "price_1H84a3KAn97x2zX0rOkE1bTz",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Weekly plan \u2013\u00a0entry point to out system, try it now",
                    "object": "price",
                    "product": "prod_HhTXEEEFNDxLOa",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "week",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 3000,
                    "unit_amount_decimal": "3000"
                },
            'price_1H84WUKAn97x2zX0WNREK4WZ':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511106,
                    "currency": "usd",
                    "id": "price_1H84WUKAn97x2zX0WNREK4WZ",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Most profitable plan for promising startups",
                    "object": "price",
                    "product": "prod_HhOWSzt9IRCfjT",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "year",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 10000,
                    "unit_amount_decimal": "10000"
                },
            'price_1H84WUKAn97x2zX0BR6tduRK':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511105,
                    "currency": "usd",
                    "id": "price_1H84WUKAn97x2zX0BR6tduRK",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Use this to try out our system with no limits",
                    "object": "price",
                    "product": "prod_HhOWSzt9IRCfjT",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "week",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 300,
                    "unit_amount_decimal": "300"
                },
            'price_1H7zjpKAn97x2zX0SAgzuJQ7':
                {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595492713,
                    "currency": "usd",
                    "id": "price_1H7zjpKAn97x2zX0SAgzuJQ7",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Common plan for short- and middle-term projects",
                    "object": "price",
                    "product": "prod_HhOWSzt9IRCfjT",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "month",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 1000,
                    "unit_amount_decimal": "1000"
                }
        }
}
