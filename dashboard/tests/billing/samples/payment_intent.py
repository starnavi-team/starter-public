payment_intent = {
    "amount": 1000,
    "amount_capturable": 0,
    "amount_received": 0,
    "application": None,
    "application_fee_amount": None,
    "canceled_at": None,
    "cancellation_reason": None,
    "capture_method": "automatic",
    "charges": {
        "data": [],
        "has_more": False,
        "object": "list",
        "total_count": 0,
        "url": "/v1/charges?payment_intent=pi_1HC0KoKAn97x2zX01R5SSeox"
    },
    "client_secret": "pi_1HC0KoKAn97x2zX01R5SSeox_secret_oq8GWm55HyYEZspYjPOJJQln4",
    "confirmation_method": "automatic",
    "created": 1596448318,
    "currency": "usd",
    "customer": None,
    "description": None,
    "id": "pi_1HC0KoKAn97x2zX01R5SSeox",
    "invoice": None,
    "last_payment_error": None,
    "livemode": False,
    "metadata": {},
    "next_action": None,
    "object": "payment_intent",
    "on_behalf_of": None,
    "payment_method": None,
    "payment_method_options": {
        "card": {
            "installments": None,
            "network": None,
            "request_three_d_secure": "automatic"
        }
    },
    "payment_method_types": [
        "card"
    ],
    "receipt_email": None,
    "review": None,
    "setup_future_usage": None,
    "shipping": None,
    "source": None,
    "statement_descriptor": None,
    "statement_descriptor_suffix": None,
    "status": "requires_payment_method",
    "transfer_data": None,
    "transfer_group": None
}
