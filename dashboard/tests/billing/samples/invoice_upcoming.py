upcoming_sample = {
    "account_country": "US",
    "account_name": "STRK",
    "amount_due": 99731,
    "amount_paid": 0,
    "amount_remaining": 99731,
    "application_fee_amount": None,
    "attempt_count": 0,
    "attempted": False,
    "billing_reason": "upcoming",
    "charge": None,
    "collection_method": "charge_automatically",
    "created": 1596708835,
    "currency": "usd",
    "custom_fields": None,
    "customer": "cus_HkReNSQ8fnGBy6",
    "customer_address": None,
    "customer_email": "pppmeter2010@gmail.com",
    "customer_name": "Artem T",
    "customer_phone": None,
    "customer_shipping": None,
    "customer_tax_exempt": "none",
    "customer_tax_ids": [],
    "default_payment_method": None,
    "default_source": None,
    "default_tax_rates": [],
    "description": None,
    "discount": None,
    "discounts": [],
    "due_date": None,
    "ending_balance": 0,
    "footer": None,
    "lines": {
        "data": [
            {
                "amount": -269,
                "currency": "usd",
                "description": "Unused time on Personal subscription after 06 Aug 2020",
                "discount_amounts": [],
                "discountable": False,
                "discounts": [],
                "id": "il_tmp1HD66hKAn97x2zX0mE25aERX",
                "invoice_item": "ii_1HD66hKAn97x2zX0mE25aERX",
                "livemode": False,
                "metadata": {},
                "object": "line_item",
                "period": {
                    "end": 1597250802,
                    "start": 1596708835
                },
                "plan": {
                    "active": True,
                    "aggregate_usage": None,
                    "amount": 300,
                    "amount_decimal": "300",
                    "billing_scheme": "per_unit",
                    "created": 1595511105,
                    "currency": "usd",
                    "id": "price_1H84WUKAn97x2zX0BR6tduRK",
                    "interval": "week",
                    "interval_count": 1,
                    "livemode": False,
                    "metadata": {},
                    "nickname": "Use this to try out our system with no limits",
                    "object": "plan",
                    "product": "prod_HhOWSzt9IRCfjT",
                    "tiers": None,
                    "tiers_mode": None,
                    "transform_usage": None,
                    "trial_period_days": None,
                    "usage_type": "licensed"
                },
                "price": {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511105,
                    "currency": "usd",
                    "id": "price_1H84WUKAn97x2zX0BR6tduRK",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Use this to try out our system with no limits",
                    "object": "price",
                    "product": "prod_HhOWSzt9IRCfjT",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "week",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 300,
                    "unit_amount_decimal": "300"
                },
                "proration": True,
                "quantity": 1,
                "subscription": "sub_HmOYo0uuMvnPz2",
                "subscription_item": "si_HmOYr2AYHcwNSI",
                "tax_amounts": [],
                "tax_rates": [],
                "type": "invoiceitem"
            },
            {
                "amount": 100000,
                "currency": "usd",
                "description": "1 \u00d7 Enterprise subscription (at $1,000.00 / year)",
                "discount_amounts": [],
                "discountable": True,
                "discounts": [],
                "id": "il_tmp_b0e23e34eb5a51",
                "livemode": False,
                "metadata": {},
                "object": "line_item",
                "period": {
                    "end": 1628244835,
                    "start": 1596708835
                },
                "plan": {
                    "active": True,
                    "aggregate_usage": None,
                    "amount": 100000,
                    "amount_decimal": "100000",
                    "billing_scheme": "per_unit",
                    "created": 1595511327,
                    "currency": "usd",
                    "id": "price_1H84a3KAn97x2zX0WM3W3j1z",
                    "interval": "year",
                    "interval_count": 1,
                    "livemode": False,
                    "metadata": {},
                    "nickname": "Yearly subscription \u2013\u00a0most efficient plan",
                    "object": "plan",
                    "product": "prod_HhTXEEEFNDxLOa",
                    "tiers": None,
                    "tiers_mode": None,
                    "transform_usage": None,
                    "trial_period_days": None,
                    "usage_type": "licensed"
                },
                "price": {
                    "active": True,
                    "billing_scheme": "per_unit",
                    "created": 1595511327,
                    "currency": "usd",
                    "id": "price_1H84a3KAn97x2zX0WM3W3j1z",
                    "livemode": False,
                    "lookup_key": None,
                    "metadata": {},
                    "nickname": "Yearly subscription \u2013\u00a0most efficient plan",
                    "object": "price",
                    "product": "prod_HhTXEEEFNDxLOa",
                    "recurring": {
                        "aggregate_usage": None,
                        "interval": "year",
                        "interval_count": 1,
                        "trial_period_days": None,
                        "usage_type": "licensed"
                    },
                    "tiers_mode": None,
                    "transform_quantity": None,
                    "type": "recurring",
                    "unit_amount": 100000,
                    "unit_amount_decimal": "100000"
                },
                "proration": False,
                "quantity": 1,
                "subscription": "sub_HmOYo0uuMvnPz2",
                "subscription_item": "si_HmOYr2AYHcwNSI",
                "tax_amounts": [],
                "tax_rates": [],
                "type": "subscription"
            }
        ],
        "has_more": False,
        "object": "list",
        "total_count": 2,
        "url": "/v1/invoices/upcoming/lines?customer=cus_HkReNSQ8fnGBy6&subscription="
               "sub_HmOYo0uuMvnPz2&subscription_proration_date=1596708835"
    },
    "livemode": False,
    "metadata": {},
    "next_payment_attempt": 1596712435,
    "number": None,
    "object": "invoice",
    "paid": False,
    "payment_intent": None,
    "period_end": 1596708835,
    "period_start": 1596708835,
    "post_payment_credit_notes_amount": 0,
    "pre_payment_credit_notes_amount": 0,
    "receipt_number": None,
    "starting_balance": 0,
    "statement_descriptor": None,
    "status": "draft",
    "status_transitions": {
        "finalized_at": None,
        "marked_uncollectible_at": None,
        "paid_at": None,
        "voided_at": None
    },
    "subscription": "sub_HmOYo0uuMvnPz2",
    "subscription_proration_date": 1596708835,
    "subtotal": 99731,
    "tax": None,
    "tax_percent": None,
    "total": 99731,
    "total_discount_amounts": [],
    "total_tax_amounts": [],
    "transfer_data": None,
    "webhooks_delivered_at": None
}
