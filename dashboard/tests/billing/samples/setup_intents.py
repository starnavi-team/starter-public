setup_intents = {
    "application": None,
    "cancellation_reason": None,
    "client_secret": "seti_1HB0aZKAn97x2zX0or8G9Qow_secret_HkVb2AhOEYp9MaBwzbQUFxAyB3pmdo1",
    "created": 1596210967,
    "customer": None,
    "description": None,
    "id": "seti_1HB0aZKAn97x2zX0or8G9Qow",
    "last_setup_error": None,
    "livemode": False,
    "mandate": None,
    "metadata": {},
    "next_action": None,
    "object": "setup_intent",
    "on_behalf_of": None,
    "payment_method": None,
    "payment_method_options": {
        "card": {
            "request_three_d_secure": "automatic"
        }
    },
    "payment_method_types": [
        "card"
    ],
    "single_use_mandate": None,
    "status": "requires_payment_method",
    "usage": "off_session"
}
