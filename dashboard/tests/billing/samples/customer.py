customers_list = {
    "data": [
        {
            "address": None,
            "balance": 0,
            "created": 1596121379,
            "currency": None,
            "default_source": None,
            "delinquent": False,
            "description": "First Last",
            "discount": None,
            "email": "test_user@example.com",
            "id": "cus_Hk7WNdETcWBxXW",
            "invoice_prefix": "ED92A557",
            "invoice_settings": {
                "custom_fields": None,
                "default_payment_method": None,
                "footer": None
            },
            "livemode": False,
            "metadata": {
                "id": "1"
            },
            "name": "First Last",
            "next_invoice_sequence": 1,
            "object": "customer",
            "phone": None,
            "preferred_locales": [],
            "shipping": None,
            "sources": {},
            "subscriptions": {},
            "tax_exempt": "none",
            "tax_ids": {}
        }
    ],
    "has_more": False,
    "object": "list",
    "url": "/v1/customers"
}
customer_collected_data = {'name': 'First Last', 'description': 'First Last', 'email': 'test_user@example.com',
                           'metadata': {'id': 1}}

single_customer = {
    "address": None,
    "balance": 0,
    "created": 1596039652,
    "currency": "usd",
    "default_source": None,
    "delinquent": False,
    "description": "DevTest",
    "discount": None,
    "email": "rValue@gmail.com",
    "id": "cus_HjlYkegzeLFAgZ",
    "invoice_prefix": "76625705",
    "invoice_settings": {
        "custom_fields": None,
        "default_payment_method": "pm_1HAI1pKAn97x2zX06r4eIUbm",
        "footer": None
    },
    "livemode": False,
    "metadata": {
        "id": "70"
    },
    "name": "DevTest",
    "next_invoice_sequence": 2,
    "object": "customer",
    "phone": None,
    "preferred_locales": [],
    "shipping": None,
    "sources": {
        "data": [],
        "has_more": False,
        "object": "list",
        "total_count": 0,
        "url": "/v1/customers/cus_HjlYkegzeLFAgZ/sources"
    },
    "subscriptions": {
        "data": [],
        "has_more": False,
        "object": "list",
        "total_count": 0,
        "url": "/v1/customers/cus_HjlYkegzeLFAgZ/subscriptions"
    },
    "tax_exempt": "none",
    "tax_ids": {
        "data": [],
        "has_more": False,
        "object": "list",
        "total_count": 0,
        "url": "/v1/customers/cus_HjlYkegzeLFAgZ/tax_ids"
    }

}
