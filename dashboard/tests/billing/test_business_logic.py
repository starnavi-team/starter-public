from unittest.mock import patch

import stripe
from stripe.error import CardError

from rest_framework.exceptions import ValidationError

from billing.models import Subscription, SubscriptionStatus, SUBSCRIPTION_ACTIVE_STATUSES, SUBSCRIPTION_FINAL_STATUSES
from .base import BaseBillingTestCase, StripeTestSamples


class SubscriptionDetailsTestCase(BaseBillingTestCase):
    def test_get_details(self):
        subscription = self.create_subscription()

        # make sample with expanded latest invoice
        sample = StripeTestSamples.get_subscription(latest_invoice=StripeTestSamples.get_invoice())

        # patch retrieve and call details property
        with patch.object(stripe.Subscription, 'retrieve', return_value=sample) as mock_method:
            # call details property
            details = subscription.details

            mock_method.assert_called_with(subscription.stripe_id, expand=('latest_invoice.payment_intent',))
            self.assertEqual(sample, details)

            # now collapse latest invoice (changed the same return_value object)
            sample['latest_invoice'] = sample['latest_invoice']['id']

            # call get_detail_info with no expand
            details = subscription.get_detail_info(expand=[])

            mock_method.assert_called_with(subscription.stripe_id, expand=[])
            self.assertEqual(sample, details)

    def test_get_billing_history(self):
        subscription = self.create_subscription()

        sample = StripeTestSamples.get_invoice(listed=True)
        with patch.object(stripe.Invoice, 'list', return_value=sample) as mock_method:
            history = subscription.get_billing_history(starting_after='some_invoice_id')
            mock_method.assert_called_with(subscription=subscription.stripe_id, starting_after='some_invoice_id')
            self.assertEqual(history, sample)


class SyncSubscriptionTestCase(BaseBillingTestCase):
    def test_one_active_subscription(self):
        sample = StripeTestSamples.get_subscription()

        def side_effect(customer, status):
            self.assertIn(status, SUBSCRIPTION_ACTIVE_STATUSES)
            self.assertNotIn(status, SUBSCRIPTION_FINAL_STATUSES)

            if status == SubscriptionStatus.active.value:
                return iter([sample])
            return iter([])

        with patch.object(stripe.Subscription, 'auto_paging_iter', side_effect=side_effect):
            Subscription.sync_user_subscription(self.user)

        self.assertEqual(Subscription.objects.count(), 1)
        subscription = Subscription.objects.get()
        self.compare_with_stripe_subscription(subscription, sample)

    def test_multiple_active_subscriptions(self):
        sample = StripeTestSamples.get_subscription()

        with patch.object(stripe.Subscription, 'auto_paging_iter', side_effect=lambda customer, status: iter([sample])):
            with self.assertRaises(RuntimeError):
                Subscription.sync_user_subscription(self.user)


class SubscribingTestCase(BaseBillingTestCase):
    def setUp(self):
        super().setUp()
        self.user.stripe_id = f'{self.user.id}_user_stripe_id'
        self.user.save()

    def test_subscribe_correct(self):
        # case where there was no subscription
        sample = StripeTestSamples.get_subscription(latest_invoice=StripeTestSamples.get_invoice())

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_sub = patch.object(stripe.Subscription, 'create', return_value=sample)

        with patch_pm as mocked_pm, patch_customer as mocked_customer, patch_sub as mocked_sub:
            # call subscribe method
            subscription, stripe_subscription = Subscription.subscribe(self.user, 'stripe_plan_id', 'stripe_payment_id')

            mocked_pm.assert_called_with('stripe_payment_id', customer=self.user.stripe_id)
            mocked_customer.assert_called_with(
                self.user.stripe_id,
                invoice_settings={'default_payment_method': 'stripe_payment_id'}
            )
            mocked_sub.assert_called_with(
                customer=self.user.stripe_id,
                items=[{'price': 'stripe_plan_id'}],
                coupon=None,
                expand=['latest_invoice', 'latest_invoice.payment_intent'],
            )

        self.assertEqual(Subscription.objects.count(), 1)
        # these are just links to the same object
        self.assertEqual(sample, stripe_subscription)
        self.compare_with_stripe_subscription(subscription, sample)

    def test_user_already_have_a_subscription(self):
        # The status of subscription doesn't matter - we just update local record with the new one. You should
        # handle custom situations before this call (override Subscription.user_can_subscribe)
        original_sub = self.create_subscription()

        sample = StripeTestSamples.get_subscription(
            latest_invoice=StripeTestSamples.get_invoice(),
            status=SubscriptionStatus.incomplete.value,  # change status to be mo clear
        )

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_sub = patch.object(stripe.Subscription, 'create', return_value=sample)

        with patch_pm, patch_customer, patch_sub:
            sub, stripe_sub = Subscription.subscribe(self.user, 'some_plan_id', 'some_payment_method_id')

        original_sub.refresh_from_db()
        self.assertEqual(Subscription.objects.count(), 1)
        self.assertEqual(original_sub, sub)
        self.compare_with_stripe_subscription(original_sub, stripe_sub)

        # now we check that original_sub = sub is updated with sample's values (they just should be different from
        # those in BaseBillingTestCase.create_subscription
        self.compare_with_stripe_subscription(original_sub, stripe_sub)

    def test_retry_subscribing(self):
        sample = StripeTestSamples.get_subscription(latest_invoice=StripeTestSamples.get_invoice())
        subscription = self.create_subscription(status=SubscriptionStatus.incomplete.value)

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_invoice = patch.object(stripe.Invoice, 'pay')
        patch_sub = patch.object(stripe.Subscription, 'retrieve', return_value=sample)

        with patch_pm as mocked_pm, \
                patch_customer as mocked_customer, \
                patch_invoice as mocked_invoice, \
                patch_sub as mocked_sub:
            stripe_sub = subscription.retry_subscribing('retry_pm_id')

            mocked_pm.assert_called_with('retry_pm_id', customer=self.user.stripe_id)
            mocked_customer.assert_called_with(
                self.user.stripe_id,
                invoice_settings={'default_payment_method': 'retry_pm_id'}
            )
            mocked_invoice.assert_called_with(sample.latest_invoice.id, payment_method='retry_pm_id')
            mocked_sub.assert_called_with(subscription.stripe_id, expand=('latest_invoice.payment_intent',))

        self.assertEqual(stripe_sub, sample)
        # check that status was updated
        subscription.refresh_from_db()
        self.assertEqual(subscription.status, stripe_sub.status)

    def test_card_error_on_subscribe(self):
        # testing wrong card
        with patch.object(stripe.PaymentMethod, 'attach', side_effect=CardError('msg', '', '')):
            try:
                Subscription.subscribe(self.user, 'any_plan', 'any_payment_method')
            except ValidationError as exc:
                self.assertEqual(
                    exc.args[0],
                    'Something is wrong with your card credentials, please provide correct ones.'
                )

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_sub = patch.object(stripe.Subscription, 'create', side_effect=CardError('charge_error', '', ''))
        # test charge error (not enough money or bank rejected the payment or stuff like this)
        with patch_pm, patch_customer, patch_sub:
            try:
                Subscription.subscribe(self.user, 'any_plan', 'any_payment_method')
            except ValidationError as exc:
                self.assertEqual(exc.args[0], 'Charging error: charge_error')

    def test_card_error_on_retry_subscribing(self):
        sample = StripeTestSamples.get_subscription(latest_invoice=StripeTestSamples.get_invoice())
        subscription = self.create_subscription(status=SubscriptionStatus.incomplete.value)
        # we have subscription and sample with different statuses because after failed payment the subscription
        # is gonna be updated with sample status
        self.assertNotEqual(subscription.status, sample.status)

        # test error on payment method management
        with patch.object(stripe.PaymentMethod, 'attach', side_effect=CardError('msg', '', '')):
            try:
                subscription.retry_subscribing('any_payment_method')
            except ValidationError as exc:
                self.assertEqual(
                    exc.args[0],
                    'Something is wrong with your card credentials, please provide correct ones.'
                )

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_invoice = patch.object(stripe.Invoice, 'pay', side_effect=CardError('invoice pay error', '', ''))
        patch_sub = patch.object(stripe.Subscription, 'retrieve', return_value=sample)
        # test error on Invoice.pay - do nothing
        with patch_pm, patch_customer, patch_invoice, patch_sub:
            stripe_subscription = subscription.retry_subscribing('any_payment_method')

        # now we check that subscription status is updated
        self.assertEqual(subscription.status, sample.status)

        # we also check what we got from the retry_subscribing call
        self.assertEqual(stripe_subscription, sample)

    def test_other_stripe_error(self):
        # For now we don't catch other type of stripe errors, because they can be caused of 2 main reasons:
        # 1. something happened with connection to stripe - user should see 500
        # 2. front got some wrong ids (plan or payment_method): use can't fix it (put correct ids) by himself so
        #    it is better to show him 500
        pass


class CancelingTestCase(BaseBillingTestCase):
    pass
