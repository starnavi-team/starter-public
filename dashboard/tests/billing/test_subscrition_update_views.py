import stripe
from accounts import models as account_models
from billing import models
from copy import deepcopy
from rest_framework.reverse import reverse
from rest_framework import status
from tests.billing.base import BaseBillingTestCase, StripeTestSamples
from tests.billing.samples import customer, cached_products_data, invoice_upcoming, pending_update_subscription
from unittest.mock import patch


class UpdateSubscriptionTestCase(BaseBillingTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        mocked_product_data = deepcopy(cached_products_data.cached_product_data)
        mocked_data = mocked_product_data['products']
        for prod_id, product in mocked_data.items():
            sample = StripeTestSamples.get_product(product)
            mocked_data[prod_id] = sample
        mocked_product_data['products'] = mocked_data

        mocked_data = mocked_product_data['prices']
        for price_id, price in mocked_data.items():
            sample = StripeTestSamples.get_price(price)
            mocked_data[price_id] = sample

        mocked_product_data['prices'] = mocked_data

        cls._patcher_get_stripe_products = patch('billing.services.get_stripe_products',
                                                 return_value=mocked_product_data)
        cls.mock_stripe_products = cls._patcher_get_stripe_products.start()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls._patcher_get_stripe_products.stop()

    def setUp(self):
        super().setUp()
        self.user.stripe_id = customer.single_customer['id']
        self.user.save()

    def test_Update_subscription_update(self):

        # ------ Create subscription ----------
        subscription_sample = StripeTestSamples.get_subscription(
            latest_invoice=StripeTestSamples.get_invoice(),
        )
        subscription_sample['items']['data'][0]['price']['id'] = 'price_1H84WUKAn97x2zX0BR6tduRK'
        subscription_sample['plan']['id'] = 'price_1H84WUKAn97x2zX0BR6tduRK'
        customer_sample = StripeTestSamples.get_customer(customer.single_customer)

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_sub = patch.object(stripe.Subscription, 'create', return_value=subscription_sample)
        patch_sync_subscriptions = patch.object(models.Subscription, 'get_detail_info', return_value=None)
        patch_subscriptions_details = patch.object(models.Subscription, 'sync_user_subscription',
                                                   return_value=subscription_sample)
        patch_sync_user = patch.object(account_models.User, 'sync_stripe_customer', return_value=customer_sample)

        with patch_pm, patch_customer, patch_sub, patch_sync_subscriptions, \
                patch_subscriptions_details, patch_sync_user:

            payload = {
                'plan_id': 'price_1H84WUKAn97x2zX0BR6tduRK',
                'payment_method_id': 'pm_1HD40CKAn97x2zX0Sco12LKg'
            }
            self.client.post(reverse('subscription-subscribe'), payload)

        self.assertEqual(models.Subscription.objects.count(), 1)
        self.assertEqual(models.Subscription.objects.all().first().stripe_price_id, 'price_1H84WUKAn97x2zX0BR6tduRK')

        # ------ Get Prorations for new plan ----------
        new_price = 'price_1H84a3KAn97x2zX0WM3W3j1z'
        invoice_upcoming_sample = StripeTestSamples.get_invoice(invoice=invoice_upcoming.upcoming_sample)
        patch_sub_rt = patch.object(stripe.Subscription, 'retrieve', return_value=subscription_sample)
        patch_invoice_uc = patch.object(stripe.Invoice, 'upcoming', return_value=invoice_upcoming_sample)

        with patch_sub_rt, patch_invoice_uc:
            payload = {
                'new_price_id': new_price
            }
            response = self.client.post(reverse('subscription_edit-get_prorations'), payload)
            self.assertEqual(response.status_code, 200)
            self.assertNotEqual(response.json().get('current_prorations'), None)

        # ------ Update Subscription ----------
        updated_subscription = StripeTestSamples.get_subscription(
            subscription=pending_update_subscription.pending_update_subscription,
            latest_invoice=StripeTestSamples.get_invoice(),
        )
        patch_sub = patch.object(stripe.Subscription, 'retrieve', return_value=subscription_sample)
        patch_sub_md = patch.object(stripe.Subscription, 'modify', return_value=updated_subscription)
        patch_get_detail = patch.object(models.Subscription, 'get_detail_info', return_value=updated_subscription)
        with patch_sub, patch_sub_md, patch_get_detail:
            payload = {
                'new_price_id': new_price
            }
            response = self.client.post(reverse('subscription_edit-upgrade'), payload)
            pending_hash = response.json()['details'].get('pending_update')
            self.assertNotEqual(pending_hash, None)
            self.assertEqual(isinstance(pending_hash, dict), True)
            self.assertNotEqual(pending_hash["expires_at"], None)

    def test_update_subscription_fail_no_subscription(self):

        # ------ Update Subscription ----------
        new_price = 'price_1H84a3KAn97x2zX0WM3W3j1z'  # this price exists
        payload = {
            'new_price_id': new_price
        }
        response = self.client.post(reverse('subscription_edit-upgrade'), payload)
        self.assertContains(response, 'There is no subscriptions to update',
                            status_code=status.HTTP_400_BAD_REQUEST)

    def test_update_subscription_fail_wrong_new_price_id(self):
        # ------ Update Subscription ----------
        new_price = 'pasdjkahsdhaskljdhaladsl'  # this price does NOT exists
        payload = {
            'new_price_id': new_price
        }
        response = self.client.post(reverse('subscription_edit-upgrade'), payload)
        self.assertContains(response, 'Wrong new price data', status_code=status.HTTP_400_BAD_REQUEST)

    def test_update_subscription_fail_downgrade_is_not_available(self):

        # ------ Create subscription ----------
        subscription_sample = StripeTestSamples.get_subscription(
            latest_invoice=StripeTestSamples.get_invoice(),
        )
        subscription_sample_price = 'price_1H84a3KAn97x2zX0WM3W3j1z'  # amount 10000
        subscription_sample['items']['data'][0]['price']['id'] = subscription_sample_price
        subscription_sample['plan']['id'] = 'price_1H84WUKAn97x2zX0BR6tduRK'
        customer_sample = StripeTestSamples.get_customer(customer.single_customer)

        patch_pm = patch.object(stripe.PaymentMethod, 'attach')
        patch_customer = patch.object(stripe.Customer, 'modify')
        patch_sub = patch.object(stripe.Subscription, 'create', return_value=subscription_sample)
        patch_sync_subscriptions = patch.object(models.Subscription, 'get_detail_info', return_value=None)
        patch_subscriptions_details = patch.object(models.Subscription, 'sync_user_subscription',
                                                   return_value=subscription_sample)
        patch_sync_user = patch.object(account_models.User, 'sync_stripe_customer', return_value=customer_sample)

        with patch_pm, patch_customer, patch_sub, patch_sync_subscriptions, \
                patch_subscriptions_details, patch_sync_user:

            payload = {
                'plan_id': subscription_sample_price,
                'payment_method_id': 'pm_1HD40CKAn97x2zX0Sco12LKg'
            }
            self.client.post(reverse('subscription-subscribe'), payload)

        self.assertEqual(models.Subscription.objects.count(), 1)
        self.assertEqual(models.Subscription.objects.all().first().stripe_price_id, subscription_sample_price)

        # ------ Update Subscription ----------
        new_price = 'price_1H84WUKAn97x2zX0BR6tduRK'  # this price does NOT exists
        payload = {
            'new_price_id': new_price
        }
        response = self.client.post(reverse('subscription_edit-upgrade'), payload)
        self.assertContains(response, 'Subscription downgrade is not available',
                            status_code=status.HTTP_400_BAD_REQUEST)
