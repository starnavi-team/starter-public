from unittest.mock import patch
import stripe
from rest_framework.reverse import reverse
from rest_framework import status
from tests.billing.base import BaseBillingTestCase, StripeTestSamples
from tests.billing.samples.invoices_list_sample import invoices_list_sample
from tests.billing.samples.cached_products_data import cached_product_data
from copy import deepcopy


class InvoiceTestCase(BaseBillingTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        mocked_product_data = deepcopy(cached_product_data)
        mocked_data = mocked_product_data['products']
        for prod_id, product in mocked_data.items():
            sample = StripeTestSamples.get_product(product)
            mocked_data[prod_id] = sample
        mocked_product_data['products'] = mocked_data

        mocked_data = mocked_product_data['prices']
        for price_id, price in mocked_data.items():
            sample = StripeTestSamples.get_price(price)
            mocked_data[price_id] = sample

        mocked_product_data['prices'] = mocked_data

        cls._patcher_get_stripe_products = patch('billing.services.get_stripe_products',
                                                 return_value=mocked_product_data)
        cls.mock_stripe_products = cls._patcher_get_stripe_products.start()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls._patcher_get_stripe_products.stop()

    def test_get_invoices_list_succes(self):

        sample = StripeTestSamples.get_invoice_list()
        with patch.object(stripe.Invoice, 'list', return_value=sample):
            response = self.client.get(reverse('invoice-list'))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            for index in range(len(invoices_list_sample)):
                self.assertEqual(invoices_list_sample[index]['number'], response.json()[index]['number'])
                self.assertEqual(invoices_list_sample[index]['invoice_pdf'], response.json()[index]['invoice_pdf'])

    def test_get_invoices_list_fail(self):
        self.user.stripe_id = ''
        self.user.save()

        sample = StripeTestSamples.get_invoice_list()
        # user field strip_id is null

        with patch.object(stripe.Invoice, 'list', return_value=sample):
            response = self.client.get(reverse('invoice-list'))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json(), [])
