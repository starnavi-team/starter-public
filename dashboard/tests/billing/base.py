import stripe
from accounts.models import User
from stripe.stripe_object import StripeObject
from stripe.util import convert_to_stripe_object
from billing.models import Subscription, SubscriptionStatus
from tests.base import get_activation_info
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from copy import deepcopy
from unittest.mock import patch
from django.test import override_settings
from .samples import subscription_sample, invoice_sample, invoices_list_sample, customer
from django.core import mail


class StripeTestSamples:
    @classmethod
    def get_subscription(cls, subscription=subscription_sample, **kwargs):
        return cls._get_sample(subscription, stripe.Subscription, **kwargs)

    @classmethod
    def get_invoice(cls, invoice=invoice_sample, **kwargs):
        return cls._get_sample(invoice, stripe.Invoice, **kwargs)

    @classmethod
    def get_product(cls, product, **kwargs):
        return cls._get_sample(product, stripe.Product, **kwargs)

    @classmethod
    def get_price(cls, price, **kwargs):
        return cls._get_sample(price, stripe.Price, **kwargs)

    @classmethod
    def get_customer(cls, customer, **kwargs):
        return cls._get_sample(customer, stripe.Customer, **kwargs)

    @classmethod
    def get_invoice_list(cls, **kwargs):
        return cls._get_sample(invoices_list_sample.invoices_list_sample, stripe.Invoice, listed=True, **kwargs)

    @classmethod
    def _get_sample(cls, sample, stripe_model, listed: bool = False, **overrides) -> StripeObject:
        data = deepcopy(sample)

        if not listed:
            data.update(overrides)
            return stripe_model.construct_from(data, key=stripe.api_key)

        data = {
            "object": "list",
            "url": "/v1/url",
            "has_more": False,
            "data": data if isinstance(data, list) else [data]
        }
        return convert_to_stripe_object(data, stripe.api_key)


@override_settings(CACHE_BACKEND='django.core.cache.backends.dummy.DummyCache')
class MockedAPITestCaseSetup(APITestCase):
    @classmethod
    def setUpClass(cls):
        customers = StripeTestSamples.get_customer(customer.customers_list)
        cls._stripe_customer_list_patcher = patch('stripe.Customer.list', return_value=customers)
        cls._stripe_subscriptions_list_patcher = patch('stripe.Subscription.auto_paging_iter', return_value=iter([]))
        cls._stripe_customer_list_patcher.start()
        cls._stripe_subscriptions_list_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls._stripe_customer_list_patcher.stop()
        cls._stripe_subscriptions_list_patcher.stop()

    def setUp(self):
        self.post_data = {
            'email': 'test_user@example.com',
            'password1': 'test12345',
            'password2': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

        response = self.client.post(reverse('rest_register'), self.post_data)

        self.user = User.objects.get(email=self.post_data.get('email'))
        key = get_activation_info(mail.outbox[0].body)
        response = self.client.post(reverse('rest_verify_email'), {'key': key})
        response = self.client.post(reverse('rest_login'), {'email': 'test_user@example.com', 'password': 'test12345'})
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {response.data["access_token"]}')


class BaseBillingTestCase(MockedAPITestCaseSetup):

    def create_subscription(self, **overrides):
        data = {
            'user': self.user,
            'stripe_id': 'stripe_id',
            'stripe_product_id': 'stripe_product_id',
            'stripe_price_id': 'stripe_price_id',
            'status': SubscriptionStatus.active.value
        }
        data.update(overrides)
        subscription = Subscription.objects.create(**data)
        return subscription

    def compare_with_stripe_subscription(self, subscription, stripe_subscription):
        self.assertEqual(subscription.stripe_id, stripe_subscription.stripe_id)
        self.assertEqual(subscription.stripe_product_id, stripe_subscription['items']['data'][0].price.product)
        self.assertEqual(subscription.stripe_price_id, stripe_subscription['items']['data'][0].price.id)
