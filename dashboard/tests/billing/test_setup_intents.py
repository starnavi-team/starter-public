from unittest.mock import patch
import stripe
from rest_framework.reverse import reverse
from rest_framework import status
from tests.billing.base import BaseBillingTestCase
from tests.billing.samples.setup_intents import setup_intents


class SetupIntentTestCase(BaseBillingTestCase):

    def test_get_SetupIntent_success(self):
        with patch.object(stripe.SetupIntent, 'create', return_value=setup_intents):
            response = self.client.get(reverse('setup_intents-list'))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(setup_intents['id'], response.json()['id'])
            self.assertEqual(setup_intents['client_secret'], response.json()['client_secret'])

    def test_get_SetupIntent_fail(self):
        self.user.stripe_id = ''
        self.user.save()
        with patch.object(stripe.SetupIntent, 'create', return_value=setup_intents):
            response = self.client.get(reverse('setup_intents-list'))
            self.assertContains(response, 'There was an error with your account',
                                status_code=status.HTTP_400_BAD_REQUEST)
