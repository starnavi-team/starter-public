from unittest.mock import patch
import stripe
from rest_framework.reverse import reverse
from rest_framework import status
from tests.billing.base import BaseBillingTestCase
from tests.billing.samples.payment_intent import payment_intent


class SetupIntentTestCase(BaseBillingTestCase):

    def test_create_payment_intent_success(self):
        self.user.stripe_id = 'cus_HjjDiWWA1I4uQi'
        self.user.save()
        payload = {
            'amount': 1000,
            'currency': 'usd'
        }
        with patch.object(stripe.PaymentIntent, 'create', return_value=payment_intent):
            response = self.client.post(reverse('payment_intent-list'), payload)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(payment_intent['id'], response.json()['id'])
            self.assertEqual(payment_intent['client_secret'], response.json()['client_secret'])

    def test_create_payment_intent_fail_no_stripe_id(self):
        self.user.stripe_id = ''
        self.user.save()
        payload = {
            'amount': 1000,
            'currency': 'usd'
        }

        with patch.object(stripe.PaymentIntent, 'create', return_value=payment_intent):
            response = self.client.post(reverse('payment_intent-list'), payload)
            self.assertContains(response, 'There was an error with your account',
                                status_code=status.HTTP_400_BAD_REQUEST)

    def test_create_payment_intent_fail_no_amount(self):
        self.user.stripe_id = 'cus_HjjDiWWA1I4uQi'
        self.user.save()
        payload = {
            # 'amount': 1000,
            'currency': 'usd'
        }
        with patch.object(stripe.PaymentIntent, 'create', return_value=payment_intent):
            response = self.client.post(reverse('payment_intent-list'), payload)
            self.assertContains(response, 'This field is required', status_code=status.HTTP_400_BAD_REQUEST)

    def test_create_payment_intent_fail_amount_is_not_integer(self):
        self.user.stripe_id = 'cus_HjjDiWWA1I4uQi'
        self.user.save()
        payload = {
            'amount': '1000fsda',
            'currency': 'usd'
        }

        with patch.object(stripe.PaymentIntent, 'create', return_value=payment_intent):
            response = self.client.post(reverse('payment_intent-list'), payload)
            self.assertContains(response, 'A valid integer is required', status_code=status.HTTP_400_BAD_REQUEST)
