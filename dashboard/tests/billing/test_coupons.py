from tests.billing import base
from rest_framework.reverse import reverse
from tests.billing.samples import coupon
from stripe.error import InvalidRequestError
from unittest.mock import patch
import stripe
from rest_framework import status


def get_coupon(coupon_id):
    if coupon_id == coupon.coupon['id']:
        return coupon.coupon

    raise InvalidRequestError(f'No such coupon:\'{coupon_id}\'', 'Request: req_iObFhHdlAmzHIc:')


class CouponsViewSetsTestCase(base.BaseBillingTestCase):

    def test_get_coupon_success(self):
        retrieve_coupon = patch.object(stripe.Coupon, 'retrieve', side_effect=get_coupon)
        with retrieve_coupon:
            response = self.client.get(reverse('coupon-list'), {'coupon_id': 'sc_strk_cp2'})
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json().get('amount_off'), coupon.coupon.get('amount_off'))

    def test_get_coupon_fail_wrong_coupon_id(self):
        retrieve_coupon = patch.object(stripe.Coupon, 'retrieve', side_effect=get_coupon)
        with retrieve_coupon:
            response = self.client.get(reverse('coupon-list'), {'coupon_id': 'sdfadsa'})
            self.assertContains(response, 'No such coupon:', status_code=status.HTTP_400_BAD_REQUEST)

    def test_get_coupon_fail_no_coupon_id(self):
        retrieve_coupon = patch.object(stripe.Coupon, 'retrieve', side_effect=get_coupon)
        with retrieve_coupon:
            response = self.client.get(reverse('coupon-list'), )
            self.assertContains(response, 'coupon_id field is a mandatory parameter',
                                status_code=status.HTTP_400_BAD_REQUEST)
