FROM python:3.8 as base

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PIPENV_SYSTEM 1

ARG BUILD_DATE

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="starterkit/django-server"

WORKDIR /opt/app

COPY Pipfile Pipfile.lock ./
RUN pip install pipenv \
 && pipenv install --system --deploy --ignore-pipfile \
 && rm Pipfile.lock

FROM base as app

EXPOSE 8000

COPY dashboard .
COPY entry_point.sh .

RUN chmod +x entry_point.sh
ENTRYPOINT ./entry_point.sh

FROM app

COPY .git ./.git
COPY .flake8 .pre-commit-config.yaml Pipfile Pipfile.lock pytest.ini ./
RUN pipenv install --system --deploy --ignore-pipfile --dev

ENTRYPOINT []
