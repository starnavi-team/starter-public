#!/bin/bash
sleep 2
python manage.py migrate
python manage.py collectstatic --no-input

gunicorn dashboard_project.wsgi:application -w 4 --bind 0.0.0.0:8000;
